﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DTO
{
    public class GetPackageDTO
    {
        public string TrackingCode { get; set; }
        public string LastCheckpoint { get; set; }
        public string Status { get; set; }
        public DateTime ReceiveDate { get; set; }
        public decimal? PackageValue { get; set; }
        public bool IsStoppedCustoms { get;set; }
        public bool HasValueToPay { get; set; }
        public decimal Weight { get; set; }
        public string WeightUnit { get; set; }
        public string PacketSize { get; set; }
        public bool IsFragile { get; set; }
    }
}
