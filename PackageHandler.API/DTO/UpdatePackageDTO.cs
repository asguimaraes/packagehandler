﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DTO
{
    public class UpdatePackageDTO
    {
        public string TrackingCode { get; set; }

        public decimal? PackageValue { get; set; }

        public int CheckpointId { get; set; }

        public bool IsStoppedCustoms { get; set; }

        public bool HasValueToPay { get; set; }

        public decimal PackageWeight { get; set; }

        public byte PackageWeightUnitId { get; set; }

        public bool IsFragile { get; set; }

        public byte PacketSizeId { get; set; }

        public byte PackageStatusId { get; set; }
    }
}
