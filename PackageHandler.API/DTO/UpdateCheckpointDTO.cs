﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DTO
{
    public class UpdateCheckpointDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public short CountryId { get; set; }
        public byte ControlTypeId { get; set; }
        public byte PlaceTypeId { get; set; }
    }
}
