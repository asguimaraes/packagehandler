﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Utils
{
    public static class Helper
    {
        public static int GetListTotalPages(int pageSize, long totalRecords)
        {
            long remainder;
            long quotient = Math.DivRem(totalRecords, pageSize, out remainder);

            int totalPages;
            if (quotient == 0)
            {
                totalPages = 1;
            }
            else
            {
               totalPages = (int)(quotient + remainder);
            }

            return totalPages;
        }
    }
}
