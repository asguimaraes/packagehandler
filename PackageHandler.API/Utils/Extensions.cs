﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Utils
{
    public static class Extensions
    {

        public static List<IEnumerable<T>> GetChunks<T>(this IEnumerable<T> collection, int chunksSize)
        {
            var collectionSize = collection.Count();
            var currentPage = 0;
            List<IEnumerable<T>> chunks = new List<IEnumerable<T>>();
            while (true)
            {
                if(currentPage >= collectionSize)
                {
                    break;
                }

                var collectionPage = collection.Skip(currentPage).Take(chunksSize);
                chunks.Add(collectionPage);
            }
            return chunks;
        }

    }
}
