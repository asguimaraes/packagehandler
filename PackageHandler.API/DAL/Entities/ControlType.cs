﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL.Entities
{
    public class ControlType
    {
        [Key]
        public byte Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public ICollection<Checkpoint> Checkpoints { get; set; }

    }
}
