﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PackageHandler.API.DAL.Entities
{
    public class PacketSize
    {
        [Key]
        public byte Id { get; set; }

        [StringLength(4)]
        public string Name { get; set; }

        public ICollection<Package> Packages { get; set; }
    }
}