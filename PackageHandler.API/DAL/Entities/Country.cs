﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PackageHandler.API.DAL.Entities
{
    public class Country
    {
        [Key]
        public short Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public short NumericCode { get; set; }

        [Required]
        [StringLength(2)]
        public string IsoAplha2Code { get; set; }

        [Required]
        [StringLength(3)]
        public string IsoAplha3Code { get; set; }

        public ICollection<Checkpoint> Checkpoints { get; set; }
    }
}