﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL.Entities
{
    public class Checkpoint 
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        public short FkCountryId { get; set; }

        public byte FkControlTypeId { get; set; }

        public byte FkPlaceTypeId { get; set; }
                
        [ForeignKey("FkCountryId")]
        public Country Country { get; set; }

        [ForeignKey("FkControlTypeId")]
        public ControlType ControlType { get; set; }

        [ForeignKey("FkPlaceTypeId")]
        public PlaceType PlaceType { get; set; }
                
        public ICollection<Package> Packages { get; set; }
    }
}
