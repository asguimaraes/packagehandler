﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PackageHandler.API.DAL.Entities
{
    public class PlaceType
    {
        [Key]
        public byte Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public ICollection<Checkpoint> Checkpoints { get; set; }
    }
}