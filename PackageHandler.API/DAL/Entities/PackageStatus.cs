﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PackageHandler.API.DAL.Entities
{
    public class PackageStatus
    {
        [Key]
        public byte Id { get; set; }

        [StringLength(25)]
        public string Name { get; set; }

        public ICollection<Package> Packages { get; set; }
                
    }
}