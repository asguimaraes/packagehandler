﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PackageHandler.API.DAL.Entities
{
    public class WeightUnit
    {
        [Key]        
        public byte Id { get; set; } 

        [StringLength(20)]
        public string Name { get; set; }

        [StringLength(6)]
        public string Abbreviation { get; set; }

        public ICollection<Package> Packages { get; set; }
    }
}