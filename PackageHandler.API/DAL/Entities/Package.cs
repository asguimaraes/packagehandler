﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PackageHandler.API.DAL.Entities
{
    [Index(nameof(TrackingCode), Name = "INDEX_TRCKNUM")]
    public class Package
    {
        [Key]
        public long Id { get; set; }

        [Required]
        [StringLength(27)]
        public string TrackingCode { get; set; }

        [Column("ReceiveDate", TypeName = "datetime")]
        public DateTime ReceiveDate { get; set; }

        [Column("PackageValue", TypeName = "numeric(11,2)")]
        public decimal? PackageValue { get; set; }

        public bool IsStoppedCustoms { get; set; }

        public bool HasValueToPay { get; set; }

        [Column("Weight", TypeName = "numeric(7,3)")]
        public decimal Weight { get; set; }

        public bool IsFragile { get; set; }

        public int FkCheckpointId { get; set; }

        public byte FkWeightUnitId { get; set; }

        public byte FkPacketSizeId { get; set; }

        public byte FkStatusId { get; set; }

        [ForeignKey("FkCheckpointId")]
        public Checkpoint Checkpoint { get; set; }

        [ForeignKey("FkWeightUnitId")]
        public WeightUnit WeightUnit { get; set; }

        [ForeignKey("FkPacketSizeId")]
        public PacketSize PacketSize { get; set; }

        [ForeignKey("FkStatusId")]
        public PackageStatus PackageStatus { get; set; }

    }
}