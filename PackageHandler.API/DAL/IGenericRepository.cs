﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IBulkOperations<TEntity> BulkOperations { set; get; }

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", int? skip = null, int? take = null);

        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "", int? skip = null, int? take = null);

        long GetCount(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "");

        Task<long> GetCountAsync(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "");

        TEntity GetByID(object id);

        Task<TEntity> GetByIDAsync(object id);

        void Insert(TEntity entity);

        Task InsertAsync(TEntity entity);

        void Update(TEntity entity);
                
        void Delete(object id);
                        
        void Delete(TEntity entity);

    }
}
