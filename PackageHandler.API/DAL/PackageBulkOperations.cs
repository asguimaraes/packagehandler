﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using PackageHandler.API.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Storage;

namespace PackageHandler.API.DAL
{
    public class PackageBulkOperations : IBulkOperations<Package>
    {
        private TrackerContext _context;

        public PackageBulkOperations(TrackerContext context)
        {
            _context = context;
        }

        public void Delete(IEnumerable<Package> packages)
        {
            var packagesDT = GetDataTable(packages);

            var currentTransaction = _context.Database.CurrentTransaction;
            var existingTransaction = currentTransaction != null;

            try
            {
                if (!existingTransaction)
                {
                    currentTransaction = _context.Database.BeginTransaction();
                }

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "dbo.SpDeletePackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var sqlParameter = new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Structured,
                        ParameterName = "@packagesToDelete",
                        Value = packagesDT
                    };

                    command.Parameters.Add(sqlParameter);
                    command.Transaction = _context.Database.CurrentTransaction.GetDbTransaction() as SqlTransaction;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        command.Connection.Open();
                    }

                    command.ExecuteNonQuery();

                    if (!existingTransaction)
                    {
                        currentTransaction.Commit();
                    }
                }

            }
            catch
            {
                if (!existingTransaction)
                {
                    currentTransaction?.Rollback();
                }

                throw;
            }
        }

        public async Task DeleteAsync(IEnumerable<Package> packages)
        {
            var packagesDT = GetDataTable(packages);

            var currentTransaction = _context.Database.CurrentTransaction;
            var existingTransaction = currentTransaction != null;

            try
            {
                if (!existingTransaction)
                {
                    currentTransaction = await _context.Database.BeginTransactionAsync();
                }

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "dbo.SpDeletePackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var sqlParameter = new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Structured,
                        ParameterName = "@packagesToDelete",
                        Value = packagesDT
                    };

                    command.Parameters.Add(sqlParameter);
                    command.Transaction = _context.Database.CurrentTransaction.GetDbTransaction() as SqlTransaction;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        await command.Connection.OpenAsync();
                    }

                    await command.ExecuteNonQueryAsync();

                    if (!existingTransaction)
                    {
                        await currentTransaction.CommitAsync();
                    }
                }

            }
            catch
            {
                if (!existingTransaction)
                {
                    await currentTransaction?.RollbackAsync();
                }

                throw;
            }
        }

        public void Insert(IEnumerable<Package> packages)
        {
            var packagesDT = GetDataTable(packages);

            var currentTransaction = _context.Database.CurrentTransaction;
            var existingTransaction = currentTransaction != null;

            try
            {
                if (!existingTransaction)
                {
                    currentTransaction = _context.Database.BeginTransaction();
                }

                using(var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "dbo.SpInsertPackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var sqlParameter = new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Structured,
                        ParameterName = "@newPackages",
                        Value = packagesDT
                    };

                    command.Parameters.Add(sqlParameter);                    
                    command.Transaction = _context.Database.CurrentTransaction.GetDbTransaction() as SqlTransaction;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        command.Connection.Open();
                    }

                    command.ExecuteNonQuery();

                    if (!existingTransaction)
                    {
                        currentTransaction.Commit();
                    }
                }
                                                                
            }
            catch
            {
                if (!existingTransaction)
                {
                    currentTransaction?.Rollback();
                }

                throw;
            }
        }

        public async Task InsertAsync(IEnumerable<Package> packages)
        {
            var packagesDT = GetDataTable(packages);

            var currentTransaction = _context.Database.CurrentTransaction;
            var existingTransaction = currentTransaction != null;

            try
            {
                if (!existingTransaction)
                {
                    currentTransaction = await _context.Database.BeginTransactionAsync();
                }

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "dbo.SpInsertPackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var sqlParameter = new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Structured,
                        ParameterName = "@newPackages",
                        Value = packagesDT
                    };

                    command.Parameters.Add(sqlParameter);
                    command.Transaction = _context.Database.CurrentTransaction.GetDbTransaction() as SqlTransaction;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        await command.Connection.OpenAsync();
                    }

                    await command.ExecuteNonQueryAsync();

                    if (!existingTransaction)
                    {
                        await currentTransaction.CommitAsync();
                    }
                }

            }
            catch
            {
                if (!existingTransaction)
                {
                    await currentTransaction?.RollbackAsync();
                }

                throw;
            }
        }

        public void Update(IEnumerable<Package> packages)
        {
            var packagesDT = GetDataTable(packages);

            var currentTransaction = _context.Database.CurrentTransaction;
            var existingTransaction = currentTransaction != null;

            try
            {
                if (!existingTransaction)
                {
                    currentTransaction = _context.Database.BeginTransaction();
                }

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "dbo.SpUpdatePackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var sqlParameter = new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Structured,
                        ParameterName = "@updatedPackages",
                        Value = packagesDT
                    };

                    command.Parameters.Add(sqlParameter);
                    command.Transaction = _context.Database.CurrentTransaction.GetDbTransaction() as SqlTransaction;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        command.Connection.Open();
                    }

                    command.ExecuteNonQuery();

                    if (!existingTransaction)
                    {
                        currentTransaction.Commit();
                    }
                }

            }
            catch
            {
                if (!existingTransaction)
                {
                    currentTransaction?.Rollback();
                }

                throw;
            }
        }

        public async Task UpdateAsync(IEnumerable<Package> packages)
        {
            var packagesDT = GetDataTable(packages);

            var currentTransaction = _context.Database.CurrentTransaction;
            var existingTransaction = currentTransaction != null;

            try
            {
                if (!existingTransaction)
                {
                    currentTransaction = await _context.Database.BeginTransactionAsync();
                }

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "dbo.SpUpdatePackages";
                    command.CommandType = CommandType.StoredProcedure;

                    var sqlParameter = new SqlParameter()
                    {
                        SqlDbType = SqlDbType.Structured,
                        ParameterName = "@updatedPackages",
                        Value = packagesDT
                    };

                    command.Parameters.Add(sqlParameter);
                    command.Transaction = _context.Database.CurrentTransaction.GetDbTransaction() as SqlTransaction;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        await command.Connection.OpenAsync();
                    }

                    await command.ExecuteNonQueryAsync();

                    if (!existingTransaction)
                    {
                        await currentTransaction.CommitAsync();
                    }
                }

            }
            catch
            {
                if (!existingTransaction)
                {
                    await currentTransaction?.RollbackAsync();
                }

                throw;
            }
        }

        private DataTable GetDataTable(IEnumerable<Package> packages)
        {
            var dt = new DataTable();
            dt.Columns.AddRange(GetDataColumns());
            SetDataRows(dt, packages);

            return dt;
        }

        private DataColumn[] GetDataColumns()
        {
            List<DataColumn> columns = new List<DataColumn>
            {                
                new DataColumn("Id", typeof(long)),
                new DataColumn("TrackingCode", typeof(string)),
                new DataColumn("ReceiveDate", typeof(DateTime)),
                new DataColumn("PackageValue", typeof(decimal)),
                new DataColumn("IsStoppedCustoms", typeof(bool)),
                new DataColumn("HasValueToPay", typeof(bool)),
                new DataColumn("Weight", typeof(decimal)),
                new DataColumn("IsFragile", typeof(bool)),
                new DataColumn("FkCheckpointId", typeof(int)),
                new DataColumn("FkWeightUnitId", typeof(short)),
                new DataColumn("FkPacketSizeId", typeof(short)),
                new DataColumn("FkStatusId", typeof(short)),
            };
                        
            return columns.ToArray();
        }

        private void SetDataRows(DataTable dt, IEnumerable<Package> packages)
        {
            foreach (var pckg in packages)
            {
                DataRow dr = dt.NewRow();
                
                dr["Id"] = pckg.Id;
                dr["TrackingCode"] = pckg.TrackingCode;
                dr["ReceiveDate"] = pckg.ReceiveDate;
                dr["PackageValue"] = pckg.PackageValue == null ? DBNull.Value : pckg.PackageValue; ;
                dr["IsStoppedCustoms"] = pckg.IsStoppedCustoms;
                dr["HasValueToPay"] = pckg.HasValueToPay;
                dr["Weight"] = pckg.Weight;
                dr["IsFragile"] = pckg.IsFragile;
                dr["FkCheckpointId"] = pckg.FkCheckpointId;
                dr["FkWeightUnitId"] = pckg.FkWeightUnitId;
                dr["FkPacketSizeId"] = pckg.FkPacketSizeId;
                dr["FkStatusId"] = pckg.FkStatusId;
                dt.Rows.Add(dr);
            }
        }
    }
}
