﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL
{
    public interface IBulkOperations<TEntity> where TEntity : class
    {
        void Insert(IEnumerable<TEntity> entities);
        Task InsertAsync(IEnumerable<TEntity> entities);

        void Update(IEnumerable<TEntity> entities);
        Task UpdateAsync(IEnumerable<TEntity> entities);

        void Delete(IEnumerable<TEntity> entities);
        Task DeleteAsync(IEnumerable<TEntity> entities);        
    }
}