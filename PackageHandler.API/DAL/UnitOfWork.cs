﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Propreties

        private TrackerContext _context;
                
        private GenericRepository<Checkpoint> _checkpointRepository;
        private GenericRepository<ControlType> _controlTypeRepository;
        private GenericRepository<Country> _countryRepository;
        private GenericRepository<Package> _packageRepository;
        private GenericRepository<PackageStatus> _packageStatusRepository;
        private GenericRepository<PacketSize> _packetSizeRepository;
        private GenericRepository<PlaceType> _placeTypeRepository;
        private GenericRepository<WeightUnit> _weightUnitRepository;

        private bool _isDisposed = false;

        #endregion

        #region Getters

        public IGenericRepository<Checkpoint> CheckpointRepository
        {
            get
            {
                if(_checkpointRepository == null)
                {
                    _checkpointRepository = new GenericRepository<Checkpoint>(_context);
                }
                return _checkpointRepository;
            }
        }
        public IGenericRepository<ControlType> ControlTypeRepository
        {
            get
            {
                if(_controlTypeRepository == null)
                {
                    _controlTypeRepository = new GenericRepository<ControlType>(_context);
                }
                return _controlTypeRepository;
            }
        }
        public IGenericRepository<Country> CountryRepository
        {
            get
            {
                if(_countryRepository == null)
                {
                    _countryRepository = new GenericRepository<Country>(_context);
                }
                return _countryRepository;
            }
        }
        public IGenericRepository<Package> PackageRepository
        {
            get
            {
                if(_packageRepository == null)
                {
                    _packageRepository = new GenericRepository<Package>(_context);
                    _packageRepository.BulkOperations = new PackageBulkOperations(_context);
                }
                return _packageRepository;
            }
        }
        public IGenericRepository<PackageStatus> PackageStatusRepository
        {
            get
            {
                if(_packageStatusRepository == null)
                {
                    _packageStatusRepository = new GenericRepository<PackageStatus>(_context);
                }
                return _packageStatusRepository;
            }
        }
        public IGenericRepository<PacketSize> PacketSizeRepository
        {
            get
            {
                if(_packetSizeRepository == null)
                {
                    _packetSizeRepository = new GenericRepository<PacketSize>(_context);
                }
                return _packetSizeRepository;
            }
        }
        public IGenericRepository<PlaceType> PlaceTypeRepository
        {
            get
            {
                if(_placeTypeRepository == null)
                {
                    _placeTypeRepository = new GenericRepository<PlaceType>(_context);
                }
                return _placeTypeRepository;
            }
        }
        public IGenericRepository<WeightUnit> WeightUnitRepository
        {
            get
            {
                if(_weightUnitRepository == null)
                {
                    _weightUnitRepository = new GenericRepository<WeightUnit>(_context);
                }
                return _weightUnitRepository;
            }
        }

        #endregion

        public UnitOfWork(TrackerContext context)
        {
            _context = context;            
        }
                
        public IDbTransaction GetTransaction()
        {
            var contextTransaction = _context.Database.BeginTransaction();
            return contextTransaction.GetDbTransaction();
        }

        public async Task<IDbTransaction> GetTransactionAsync()
        {
            var contextTransaction = await _context.Database.BeginTransactionAsync();
            return contextTransaction.GetDbTransaction();            
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
                
    }
}
