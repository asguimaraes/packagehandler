﻿using Microsoft.EntityFrameworkCore;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL
{
    public class TrackerContext : DbContext
    {
        public DbSet<Package> Packages { get; set; }
        public DbSet<Checkpoint> Checkpoints { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<ControlType> ControlTypes { get; set; }
        public DbSet<PlaceType> PlaceTypes { get; set; }
        public DbSet<WeightUnit> WeightUnits { get; set; }
        public DbSet<PacketSize> PacketSizes { get; set; }
        public DbSet<PackageStatus> PackageStatuses { get; set; }
        
        public TrackerContext(DbContextOptions<TrackerContext> options) : base(options)
        {

        }

    }
}
