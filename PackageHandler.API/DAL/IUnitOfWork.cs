﻿using Microsoft.EntityFrameworkCore.Storage;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Checkpoint> CheckpointRepository { get; } 
        IGenericRepository<ControlType> ControlTypeRepository { get; }
        IGenericRepository<Country> CountryRepository { get; }
        IGenericRepository<Package> PackageRepository { get; }
        IGenericRepository<PackageStatus> PackageStatusRepository { get; }
        IGenericRepository<PacketSize> PacketSizeRepository { get; }
        IGenericRepository<PlaceType> PlaceTypeRepository { get; }
        IGenericRepository<WeightUnit> WeightUnitRepository { get; }

        IDbTransaction GetTransaction();

        Task<IDbTransaction> GetTransactionAsync();
                
        void Save();

        Task SaveAsync();

    }
}
