﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IPaginatable
    {
        int GetNextPage();
        long GetTotalRecords();
    }
}
