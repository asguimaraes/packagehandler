﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IDeleteCheckpointAsync : IValidatable
    {
        Task DeleteCheckpointAsync(int id);
    }
}
