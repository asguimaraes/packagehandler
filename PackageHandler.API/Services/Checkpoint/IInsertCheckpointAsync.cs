﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IInsertCheckpointAsync<T> : IValidatable where T : class
    {
        Task CreateCheckpointAsync(T package);
    }
}
