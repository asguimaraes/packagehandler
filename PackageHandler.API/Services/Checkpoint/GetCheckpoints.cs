﻿using Microsoft.Extensions.Configuration;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using PackageHandler.API.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class GetCheckpoints : IGetCheckpointAsync<GetCheckpointDTO>
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;
        private int _nextPage;
        private long _totalRecords;

        public GetCheckpoints(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        public async Task<IEnumerable<GetCheckpointDTO>> GetCheckpointsById(IEnumerable<int> ids, int page = 1, int? pageSize = null)
        {
            List<GetCheckpointDTO> returnedPackages = new List<GetCheckpointDTO>();

            if (page >= 1)
            {
                var usedPageSize = GetQueryPageSize(pageSize);

                var packages = await _unitOfWork.CheckpointRepository.GetAsync(filter: c => ids.Contains(c.Id),                    
                    includeProperties: "Country,ControlType,PlaceType",
                    skip: (page - 1), take: usedPageSize);

                var count = await _unitOfWork.CheckpointRepository.GetCountAsync(filter: p => ids.Contains(p.Id));
                _nextPage = CalcNextPage(page, usedPageSize, count);
                _totalRecords = count;

                returnedPackages = ParseCheckpointList(packages).ToList();
            }
            else
            {
                _nextPage = -1;
                _totalRecords = 0;
            }

            return returnedPackages;
        }

        public async Task<IEnumerable<GetCheckpointDTO>> GetCheckpointsByCountry(string countryIsoCode, int page = 1, int? pageSize = null)
        {
            List<GetCheckpointDTO> returnedPackages = new List<GetCheckpointDTO>();

            if (page >= 1)
            {
                var usedPageSize = GetQueryPageSize(pageSize);

                var packages = await _unitOfWork.CheckpointRepository.GetAsync(filter: c => c.Country.Name == countryIsoCode,
                    includeProperties: "Country,ControlType,PlaceType",
                    skip: (page - 1), take: usedPageSize);

                var count = await _unitOfWork.CheckpointRepository.GetCountAsync(filter: c => c.Country.Name == countryIsoCode);
                _nextPage = CalcNextPage(page, usedPageSize, count);
                _totalRecords = count;

                returnedPackages = ParseCheckpointList(packages).ToList();
            }
            else
            {
                _nextPage = -1;
                _totalRecords = 0;
            }

            return returnedPackages;
        }

        public IEnumerable<GetCheckpointDTO> ParseCheckpointList(IEnumerable<Checkpoint> checkpoints)
        {
            return checkpoints.Select(p => new GetCheckpointDTO
            {
                Id = p.Id,
                City = p.City,
                ControlType = p.ControlType.Name,
                Country = p.Country.Name,
                Name = p.Name,
                PlaceType = p.PlaceType.Name
            });
        }

        public int CalcNextPage(int currentPage, int pageSize, long totalRecords)
        {
            int totalPages = Helper.GetListTotalPages(pageSize, totalRecords);
            int nextPage = currentPage + 1;
            if (nextPage >= totalPages)
            {
                return -1;
            }
            return nextPage;
        }

        public int GetQueryPageSize(int? pageSize)
        {
            var defaultPageSize = GetDefaultPageSize();
            if (pageSize == null || pageSize > defaultPageSize)
            {
                return defaultPageSize;
            }
            else
            {
                return pageSize.Value;
            }
        }

        public int GetDefaultPageSize()
        {
            int receivedStatusId = int.Parse(_configuration["TrackerAPISettings:DefaultPageSize"]);
            return receivedStatusId;
        }

        public int GetNextPage()
        {
            return _nextPage;
        }

        public long GetTotalRecords()
        {
            return _totalRecords;
        }
    }
}
