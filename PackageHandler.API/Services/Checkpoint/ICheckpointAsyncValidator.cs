﻿using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface ICheckpointAsyncValidator : IValidatable
    {
        Task ValidateCheckpointAsync(Checkpoint checkpoint);
    }

}
