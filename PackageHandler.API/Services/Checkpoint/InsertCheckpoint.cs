﻿using Microsoft.Extensions.Configuration;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class InsertCheckpoint : IInsertCheckpointAsync<InsertCheckpointDTO>
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;        
        private ICheckpointAsyncValidator _checkpointValidator;

        public InsertCheckpoint(IUnitOfWork unitOfWork, ICheckpointAsyncValidator checkpointValidator)
        {
            _unitOfWork = unitOfWork;
            _checkpointValidator = checkpointValidator;
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
        }

        public async Task CreateCheckpointAsync(InsertCheckpointDTO checkpointDTO)
        {
            var checkpoint = GetNewCheckpoint(checkpointDTO);

            await ValidateCreation(checkpoint);

            if (!_isValid)
            {
                return;
            }
                        
            await InsertPackageToDb(checkpoint);
            
        }
        
        public async Task InsertPackageToDb(Checkpoint checkpoint)
        {
            await _unitOfWork.CheckpointRepository.InsertAsync(checkpoint);
            await _unitOfWork.SaveAsync();
        }
                
        public async Task ValidateCreation(Checkpoint checkpoint)
        {            
            await _checkpointValidator.ValidateCheckpointAsync(checkpoint);
            if (!_checkpointValidator.IsValid())
            {
                _isValid = false;
                _validationErrors = _checkpointValidator.GetValidationErrors();
            }
        }

        public Checkpoint GetNewCheckpoint(InsertCheckpointDTO packageDTO)
        {
            var checkpoint = new Checkpoint
            {
                Name = packageDTO.Name,
                City = packageDTO.City,
                FkCountryId = packageDTO.CountryId,
                FkControlTypeId = packageDTO.ControlTypeId,
                FkPlaceTypeId = packageDTO.PlaceTypeId
            };

            return checkpoint;
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }
    }
}
