﻿using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class DeleteCheckpoint : IDeleteCheckpointAsync
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;


        public DeleteCheckpoint(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
        }

        public async Task DeleteCheckpointAsync(int id)
        {
            var checkpoint = await GetCheckpointByTrackingCode(id);

            ValidateDeletion(checkpoint);

            if (!_isValid)
            {
                return;
            }

            await DeleteCheckpointOnDb(checkpoint);
        }

        public async Task DeleteCheckpointOnDb(Checkpoint checkpoint)
        {
            _unitOfWork.CheckpointRepository.Delete(checkpoint);
            await _unitOfWork.SaveAsync();
        }

        public void ValidateDeletion(Checkpoint checkpoint)
        {
            if (checkpoint == null)
            {
                _isValid = false;
                _validationErrors.Add($"Id",
                    "No checkpoint found with the provided id");
            }
        }

        public async Task<Checkpoint> GetCheckpointByTrackingCode(int id)
        {
            return (await _unitOfWork.CheckpointRepository
                .GetAsync(filter: p => p.Id == id))
                .FirstOrDefault();
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }
    }
}
