﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IUpdateCheckpointAsync<T> : IValidatable where T : class
    {
        Task UpdateCheckpointAsync(T package);
    }
}
