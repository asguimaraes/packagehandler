﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IGetCheckpointAsync<T> : IPaginatable where T : class
    {
        Task<IEnumerable<T>> GetCheckpointsById(IEnumerable<int> ids, int page = 1, int? pageSize = null);
        Task<IEnumerable<T>> GetCheckpointsByCountry(string countryIsoCode, int page = 1, int? pageSize = null);
    }
}
