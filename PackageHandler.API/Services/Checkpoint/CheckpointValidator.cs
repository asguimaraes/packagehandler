﻿using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class CheckpointValidator : ICheckpointAsyncValidator
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;

        public CheckpointValidator(IUnitOfWork unitOfWork)
        {
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
            _unitOfWork = unitOfWork;
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }


        public async Task ValidateCheckpointAsync(Checkpoint checkpoint)
        {
            if (!ValidateCheckpointName(checkpoint.Name))
            {
                _isValid = false;
                _validationErrors.Add($"Name",
                    "Name length must be > 0 and <= 100");
                return;
            }

            if (!ValidateCheckpointCity(checkpoint.City))
            {
                _isValid = false;
                _validationErrors.Add($"Name",
                    "Name length must be > 0 and <= 100");
                return;
            }

            if (!await ValidateCheckpointCountry(checkpoint.FkCountryId))
            {
                _isValid = false;
                _validationErrors.Add($"Country id",
                    "Invalid id");
                return;
            }

            if (!await ValidateControlType(checkpoint.FkControlTypeId))
            {
                _isValid = false;
                _validationErrors.Add($"Control type id",
                    "Invalid id");
                return;
            }

            if (!await ValidatePlaceType(checkpoint.FkPlaceTypeId))
            {
                _isValid = false;
                _validationErrors.Add($"Place type id",
                    "Invalid id");
                return;
            }
        }

        public bool ValidateCheckpointName(string name)
        {
            var nameLength = name?.Length ?? 0;
            if (nameLength == 0 || nameLength > 100)
            {
                return false;
            }

            return true;
        }
        
        public bool ValidateCheckpointCity(string city)
        {
            var cityLength = city?.Length ?? 0;
            if (cityLength == 0 || cityLength > 100)
            {
                return false;
            }

            return true;
        }
        
        public async Task<bool> ValidateCheckpointCountry(short countryId)
        {
            var countryRepository = _unitOfWork.CountryRepository;
            var country = await countryRepository.GetByIDAsync(countryId);
            if (country == null)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> ValidateControlType(byte controlTypeId)
        {
            var controlTypeRepository = _unitOfWork.ControlTypeRepository;
            var controlType = await controlTypeRepository.GetByIDAsync(controlTypeId);
            if (controlType == null)
            {
                return false;
            }
            return true;
        }
        
        public async Task<bool> ValidatePlaceType(byte placeTypeId)
        {
            var placeTypeRepository = _unitOfWork.PlaceTypeRepository;
            var placeType = await placeTypeRepository.GetByIDAsync(placeTypeId);
            if (placeType == null)
            {
                return false;
            }
            return true;
        }
    }
}
