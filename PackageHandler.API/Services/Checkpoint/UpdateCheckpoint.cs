﻿using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class UpdateCheckpoint : IUpdateCheckpointAsync<UpdateCheckpointDTO>
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;
        private ICheckpointAsyncValidator _checkpointValidator;

        public UpdateCheckpoint(IUnitOfWork unitOfWork, ICheckpointAsyncValidator checkpointValidator)
        {
            _unitOfWork = unitOfWork;            
            _checkpointValidator = checkpointValidator;
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
        }

        public async Task UpdateCheckpointAsync(UpdateCheckpointDTO checkpointDTO)
        {
            var checkpoint = await GetCheckpointById(checkpointDTO.Id);

            UpdateCheckpointFields(checkpointDTO, checkpoint);

            await ValidateUpdate(checkpoint);

            if (!_isValid)
            {
                return;
            }

            await UpdateCheckpointOnDb(checkpoint);
        }

        public async Task UpdateCheckpointOnDb(Checkpoint checkpoint)
        {
            _unitOfWork.CheckpointRepository.Update(checkpoint);
            await _unitOfWork.SaveAsync();
        }

        public async Task ValidateUpdate(Checkpoint checkpoint)
        {
            if (checkpoint == null)
            {
                _isValid = false;
                _validationErrors.Add($"Id",
                    "No checkpoint found with the provided id");
                return;
            }

            await _checkpointValidator.ValidateCheckpointAsync(checkpoint);
            if (!_checkpointValidator.IsValid())
            {
                _isValid = false;
                _validationErrors = _checkpointValidator.GetValidationErrors();
            }
        }

        public async Task<Checkpoint> GetCheckpointById(int id)
        {
            return (await _unitOfWork.CheckpointRepository
                .GetAsync(filter: p => p.Id == id))
                .FirstOrDefault();
        }

        public void UpdateCheckpointFields(UpdateCheckpointDTO checkpointDTO, Checkpoint checkpoint)
        {
            if (checkpoint != null)
            {
                checkpoint.Name = checkpointDTO.Name;
                checkpoint.City = checkpointDTO.City;
                checkpoint.FkCountryId = checkpointDTO.CountryId;
                checkpoint.FkControlTypeId = checkpointDTO.ControlTypeId;
                checkpoint.FkPlaceTypeId = checkpointDTO.PlaceTypeId;
            }
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }
    }
}
