﻿using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class PackageValidator : IPackageAsyncValidator
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;

        public PackageValidator(IUnitOfWork unitOfWork)
        {
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
            _unitOfWork = unitOfWork;
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }

        
        public async Task ValidatePackageAsync(Package package, int? packageIndex = null)
        {
            if (!ValidatePackageValue(package.PackageValue))
            {
                _isValid = false;
                _validationErrors.Add($"Package value{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    $"Value must be less or equal {999999999.99M: N2}");
                return;
            }

            if (!ValidatePackageWeight(package.Weight))
            {
                _isValid = false;
                _validationErrors.Add($"Package weight{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    $"Value must be less or equal {999999999.99M: N2}");
                return;
            }

            if (!await ValidatePackageCheckpoint(package.FkCheckpointId))
            {
                _isValid = false;
                _validationErrors.Add($"Checkpoint Id{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Invalid id");
                return;
            }

            if (!await ValidatePackageWeightUnit(package.FkWeightUnitId))
            {
                _isValid = false;
                _validationErrors.Add($"Weight unit Id{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Invalid id");
                return;
            }

            if (!await ValidatePacketSize(package.FkPacketSizeId))
            {
                _isValid = false;
                _validationErrors.Add($"Packet size Id{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Invalid id");
                return;
            }

            if (!await ValidatePackageStatus(package.FkStatusId))
            {
                _isValid = false;
                _validationErrors.Add($"Package status Id{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Invalid id");
                return;
            }
        }
                
        public bool ValidatePackageValue(decimal? packageValue)
        {
            if ((packageValue ?? 0M) > 999999999.99M)
            {
                return false;
            }

            return true;
        }

        public bool ValidatePackageWeight(decimal packageWeight)
        {
            if (packageWeight > 9999.999M)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ValidatePackageCheckpoint(int packageCheckpoint)
        {
            var checkpointRepository = _unitOfWork.CheckpointRepository;
            var checkpoint = await checkpointRepository.GetByIDAsync(packageCheckpoint);
            if (checkpoint == null)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> ValidatePackageWeightUnit(byte packageWeightUnit)
        {
            var weightUnitRepository = _unitOfWork.WeightUnitRepository;
            var weightUnit = await weightUnitRepository.GetByIDAsync(packageWeightUnit);
            if (weightUnit == null)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> ValidatePacketSize(byte packagePacketSize)
        {
            var packetSizeRepository = _unitOfWork.PacketSizeRepository;
            var packetSize = await packetSizeRepository.GetByIDAsync(packagePacketSize);
            if (packetSize == null)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> ValidatePackageStatus(byte packageStatus)
        {
            var packageStatusRepository = _unitOfWork.PackageStatusRepository;
            var packageSatus = await packageStatusRepository.GetByIDAsync(packageStatus);
            if (packageSatus == null)
            {
                return false;
            }
            return true;
        }
    }
}
