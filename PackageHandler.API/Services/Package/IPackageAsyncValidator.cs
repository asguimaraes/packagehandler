﻿using PackageHandler.API.DAL.Entities;
using PackageHandler.API.Services;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IPackageAsyncValidator : IValidatable
    {
        Task ValidatePackageAsync(Package package, int? packageIndex = null);

    }
}
