﻿using PackageHandler.API.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IGetPackagesAsync<T> : IPaginatable where T :class
    {        
        Task<IEnumerable<T>> GetPackagesByTrackingCodes(IEnumerable<string> trackingCodes, int page = 1, int? pageSize = null);        
        Task<IEnumerable<T>> GetPackagesByStatus(byte statusId, int page = 1, int? pageSize = null);
        Task<IEnumerable<T>> GetPackagesByCountry(string isoCode, int page = 1, int? pageSize = null);
        Task<decimal> GetInTransitValue();
    }
}
