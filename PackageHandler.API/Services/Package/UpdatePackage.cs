﻿using Microsoft.Extensions.Configuration;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class UpdatePackage : IUpdatePackageAsync<UpdatePackageDTO>
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;
        private IConfiguration _configuration;
        private IPackageAsyncValidator _packageValidator;

        public UpdatePackage(IUnitOfWork unitOfWork, IConfiguration configuration, IPackageAsyncValidator packageValidator)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _packageValidator = packageValidator;
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
        }

        #region Methods

        #region Update

        public async Task UpdatePackageAsync(UpdatePackageDTO packageDTO)
        {
            var package = await GetPackageByTrackingCode(packageDTO.TrackingCode);

            UpdatePackageFields(packageDTO, package);

            await ValidateUpdate(package);

            if (!_isValid)
            {
                return;
            }

            await UpdatePackageOnDb(package);
        }

        public async Task UpdatePackageAsync(IEnumerable<UpdatePackageDTO> packagesDTOs)
        {
            var packages = new List<Package>();

            var pckgIndex = 0;
            foreach (var packageDTO in packagesDTOs)
            {
                if (!_isValid)
                {
                    break;
                }

                var package = await GetPackageByTrackingCode(packageDTO.TrackingCode);

                UpdatePackageFields(packageDTO, package);

                await ValidateUpdate(package, pckgIndex);

                if (!_isValid)
                {
                    break; ;
                }

                packages.Add(package);
                pckgIndex++;
            }

            if (!_isValid || packages.Count() == 0)
            {
                return;
            }

            await UpdatePackageOnDb(packages);
        }

        public async Task UpdatePackageOnDb(Package package)
        {
            _unitOfWork.PackageRepository.Update(package);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdatePackageOnDb(IEnumerable<Package> packages)
        {
            await _unitOfWork.PackageRepository.BulkOperations.UpdateAsync(packages);
        }

        #endregion

        #region Validation

        public async Task ValidateUpdate(Package package, int? packageIndex = null)
        {
            if (package == null)
            {
                _isValid = false;
                _validationErrors.Add($"Tracking code{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "No package found with the provided tracking code");
                return;
            }

            await _packageValidator.ValidatePackageAsync(package, packageIndex);
            if (!_packageValidator.IsValid())
            {
                _isValid = false;
                _validationErrors = _packageValidator.GetValidationErrors();
            }
        }

        #endregion

        public async Task<Package> GetPackageByTrackingCode(string trackingCode)
        {
            return (await _unitOfWork.PackageRepository
                .GetAsync(filter: p => p.TrackingCode.ToUpper() == trackingCode.ToUpper()))
                .FirstOrDefault();
        }

        public void UpdatePackageFields(UpdatePackageDTO packageDTO, Package package)
        {
            if (package != null)
            {
                package.PackageValue = packageDTO.PackageValue;
                package.IsStoppedCustoms = packageDTO.IsStoppedCustoms;
                package.HasValueToPay = packageDTO.HasValueToPay;
                package.Weight = packageDTO.PackageWeight;
                package.IsFragile = packageDTO.IsFragile;
                package.FkCheckpointId = packageDTO.CheckpointId;
                package.FkWeightUnitId = packageDTO.PackageWeightUnitId;
                package.FkPacketSizeId = packageDTO.PacketSizeId;
                package.FkStatusId = packageDTO.PackageStatusId;
            }
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }

        #endregion
    }
}
