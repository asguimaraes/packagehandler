﻿using Microsoft.AspNetCore.Http;
using PackageHandler.API.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IInsertPackageAsync<T> : IValidatable where T : class
    {
        Task<string> CreatePackageAsync(T package);
        Task<List<string>> CreatePackageAsync(IEnumerable<T> packages);
        Task<List<string>> CreatePackagesFromXlsAsync(IFormFile xlsPostedFile);
    }
}
