﻿using Microsoft.Extensions.Configuration;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using PackageHandler.API.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class GetPackages : IGetPackagesAsync<GetPackageDTO>
    {
        private IUnitOfWork _unitOfWork;        
        private IConfiguration _configuration;
        private int _nextPage;
        private long _totalRecords;

        public GetPackages(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        public async Task<IEnumerable<GetPackageDTO>> GetPackagesByTrackingCodes(IEnumerable<string> trackingCodes, int page = 1, int? pageSize = null)
        {
            List<GetPackageDTO> returnedPackages = new List<GetPackageDTO>();

            if(page >= 1)
            {
                var usedPageSize = GetQueryPageSize(pageSize);

                var packages = await _unitOfWork.PackageRepository.GetAsync(filter: p => trackingCodes.Contains(p.TrackingCode),
                    orderBy: o => o.OrderByDescending(d => d.ReceiveDate),
                    includeProperties: "Checkpoint,WeightUnit,PacketSize,PackageStatus",
                    skip: (page-1), take: usedPageSize);

                var count = await _unitOfWork.PackageRepository.GetCountAsync(filter: p => trackingCodes.Contains(p.TrackingCode));
                _nextPage = CalcNextPage(page, usedPageSize, count);
                _totalRecords = count;

                returnedPackages = ParsePackageList(packages).ToList();
            }
            else
            {
                _nextPage = -1;
                _totalRecords = 0;
            }

            return returnedPackages;
        }

        public async Task<IEnumerable<GetPackageDTO>> GetPackagesByStatus(byte statusId, int page = 1, int? pageSize = null)
        {
            List<GetPackageDTO> returnedPackages = new List<GetPackageDTO>();

            if (page >= 1)
            {
                var usedPageSize = GetQueryPageSize(pageSize);

                var packages = await _unitOfWork.PackageRepository.GetAsync(filter: p => p.FkStatusId == statusId,
                    orderBy: o => o.OrderByDescending(d => d.ReceiveDate),
                    includeProperties: "Checkpoint,WeightUnit,PacketSize,PackageStatus",
                    skip: (page - 1), take: usedPageSize);

                var count = await _unitOfWork.PackageRepository.GetCountAsync(filter: p => p.FkStatusId == statusId);
                _nextPage = CalcNextPage(page, usedPageSize, count);
                _totalRecords = count;

                returnedPackages = ParsePackageList(packages).ToList();
            }
            else
            {
                _nextPage = -1;
                _totalRecords = 0;
            }

            return returnedPackages;
        }

        public async Task<IEnumerable<GetPackageDTO>> GetPackagesByCountry(string isoCode, int page = 1, int? pageSize = null)
        {
            List<GetPackageDTO> returnedPackages = new List<GetPackageDTO>();

            if (page >= 1)
            {
                var usedPageSize = GetQueryPageSize(pageSize);

                var packages = await _unitOfWork.PackageRepository.GetAsync(filter: p => p.Checkpoint.Country.IsoAplha2Code.ToUpper() == isoCode.ToUpper(),
                    orderBy: o => o.OrderByDescending(d => d.ReceiveDate),
                    includeProperties: "Checkpoint.Country,WeightUnit,PacketSize,PackageStatus",
                    skip: (page - 1), take: usedPageSize);

                var count = await _unitOfWork.PackageRepository.GetCountAsync(filter: p => p.Checkpoint.Country.IsoAplha2Code == isoCode,
                    includeProperties: "Checkpoint.Country,WeightUnit,PacketSize,PackageStatus");
                _nextPage = CalcNextPage(page, usedPageSize, count);
                _totalRecords = count;

                returnedPackages = ParsePackageList(packages).ToList();
            }
            else
            {
                _nextPage = -1;
                _totalRecords = 0;
            }

            return returnedPackages;
        }

        public async Task<decimal> GetInTransitValue()
        {
            var inTransitStatusId = GetInTransitStatusId();

            var packages = await _unitOfWork.PackageRepository.GetAsync(filter: p => p.FkPacketSizeId == inTransitStatusId);

            return packages.Select(p => p.PackageValue.GetValueOrDefault()).Sum();
        }

        public IEnumerable<GetPackageDTO> ParsePackageList(IEnumerable<Package> packages)
        {
            return packages.Select(p => new GetPackageDTO
            {
                TrackingCode = p.TrackingCode,
                HasValueToPay = p.HasValueToPay,
                IsFragile = p.IsFragile,
                IsStoppedCustoms = p.IsStoppedCustoms,
                LastCheckpoint = p.Checkpoint.Name,
                PackageValue = p.PackageValue,
                PacketSize = p.PacketSize.Name,
                ReceiveDate = p.ReceiveDate,
                Status = p.PackageStatus.Name,
                Weight = p.Weight,
                WeightUnit = p.WeightUnit.Abbreviation
            });
        }

        public int CalcNextPage(int currentPage, int pageSize, long totalRecords)
        {
            int totalPages = Helper.GetListTotalPages(pageSize, totalRecords);
            int nextPage = currentPage + 1;   
            if(nextPage >= totalPages)
            {
                return -1;
            }
            return nextPage;
        }

        public int GetQueryPageSize(int? pageSize)
        {
            var defaultPageSize = GetDefaultPageSize();
            if(pageSize == null || pageSize > defaultPageSize)
            {
                return defaultPageSize;
            }
            else
            {
                return pageSize.Value;
            }
        }

        public int GetDefaultPageSize()
        {
            int receivedStatusId = int.Parse(_configuration["TrackerAPISettings:DefaultPageSize"]);
            return receivedStatusId;
        }

        public int GetNextPage()
        {
            return _nextPage;
        }
        
        public long GetTotalRecords()
        {
            return _totalRecords;
        }

        public byte GetInTransitStatusId()
        {
            byte receivedStatusId = byte.Parse(_configuration["TrackerAPISettings:InTransitStatusId"]);
            return receivedStatusId;
        }
    }
}
