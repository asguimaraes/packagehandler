﻿using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class InsertPackage : IInsertPackageAsync<InsertPackageDTO>
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;
        private IConfiguration _configuration;
        private IPackageAsyncValidator _packageValidator;

        public InsertPackage(IUnitOfWork unitOfWork, IConfiguration configuration, IPackageAsyncValidator packageValidator)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _packageValidator = packageValidator;
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
        }

        #region Methods

        #region Insert

        public async Task<string> CreatePackageAsync(InsertPackageDTO packageDTO)
        {
            var package = GetNewPackage(packageDTO);

            await ValidateCreation(packageDTO, package);

            if (!_isValid)
            {
                return string.Empty;
            }

            package.TrackingCode = await GetNewTrackingCode(packageDTO.IsoCountryCode, packageDTO.DeliveryArea, package.ReceiveDate);
            await InsertPackageToDb(package);
            return package.TrackingCode;
        }

        public async Task<List<string>> CreatePackageAsync(IEnumerable<InsertPackageDTO> packagesDTOs)
        {
            var packages = new List<Package>();
            var trackingCodes = new List<string>();

            var pckgIndex = 0;
            foreach (var packageDTO in packagesDTOs)
            {
                if (!_isValid)
                {
                    break;
                }

                var package = GetNewPackage(packageDTO);

                await ValidateCreation(packageDTO, package, pckgIndex);

                if (!_isValid)
                {
                    break;
                }

                package.TrackingCode = await GetNewTrackingCode(packageDTO.IsoCountryCode, packageDTO.DeliveryArea, package.ReceiveDate);
                packages.Add(package);
                trackingCodes.Add(package.TrackingCode);
                pckgIndex++;
            }

            if (!_isValid || trackingCodes.Count() == 0)
            {
                return trackingCodes;
            }

            await InsertPackageToDb(packages);

            return trackingCodes;

        }

        public async Task<List<string>> CreatePackagesFromXlsAsync(IFormFile xlsPostedFile)
        {
            var xlsPackages = ParseXlsFile(xlsPostedFile);
            return await CreatePackageAsync(xlsPackages);
        }

        public async Task InsertPackageToDb(Package package)
        {
            await _unitOfWork.PackageRepository.InsertAsync(package);
            await _unitOfWork.SaveAsync();
        }

        public async Task InsertPackageToDb(IEnumerable<Package> packages)
        {
            await _unitOfWork.PackageRepository.BulkOperations.InsertAsync(packages);
        }

        #endregion

        #region Validation

        public async Task ValidateCreation(InsertPackageDTO packageDTO, Package package, int? packageIndex = null)
        {
            if (!ValidatePackageCountryCode(packageDTO.IsoCountryCode))
            {
                _isValid = false;
                _validationErrors.Add($"Iso country code{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Iso country code length must be = 2");
                return;
            }

            if (!await ValidatePackageCountry(packageDTO.IsoCountryCode))
            {
                _isValid = false;
                _validationErrors.Add($"Iso country code{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Invalid id");
                return;
            }

            if (!ValidatePackageDeliveryArea(packageDTO.DeliveryArea))
            {
                _isValid = false;
                _validationErrors.Add($"Delivery area{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "Delivery area length must be >= 1 and <= 7");
                return;
            }

            await _packageValidator.ValidatePackageAsync(package, packageIndex);
            if (!_packageValidator.IsValid())
            {
                _isValid = false;
                _validationErrors = _packageValidator.GetValidationErrors();
            }
        }

        public bool ValidatePackageCountryCode(string packagIsoCountryCode)
        {            
            if ((packagIsoCountryCode?.Length ?? 0) != 2)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ValidatePackageCountry(string packagIsoCountryCode)
        {
            var countryRepository = _unitOfWork.CountryRepository;
            var countries = await countryRepository.GetAsync(filter: c => c.IsoAplha2Code.ToUpper() == packagIsoCountryCode.ToUpper());
            if (countries.FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        public bool ValidatePackageDeliveryArea(string deliveryArea)
        {
            var deliveryAreaLength = deliveryArea?.Length ?? 0;
            if (deliveryAreaLength < 1 || deliveryAreaLength > 7)
            {
                return false;
            }

            return true;
        }

        #endregion

        public Package GetNewPackage(InsertPackageDTO packageDTO)
        {
            var package = new Package
            {
                ReceiveDate = packageDTO.ReceiveDate,
                PackageValue = packageDTO.PackageValue,
                IsStoppedCustoms = packageDTO.IsStoppedCustoms,
                HasValueToPay = packageDTO.HasValueToPay,
                Weight = packageDTO.PackageWeight,
                IsFragile = packageDTO.IsFragile,
                FkCheckpointId = packageDTO.CheckpointId,
                FkWeightUnitId = packageDTO.PackageWeightUnitId,
                FkPacketSizeId = packageDTO.PacketSizeId,
                FkStatusId = GetReceivedStatusId()
            };

            return package;
        }

        #region Tracking code

        public async Task<string> GetNewTrackingCode(string countryIsoCode, string deliveryArea, DateTime receiveDate)
        {
            var numericCode = await GetCountryNumericCode(countryIsoCode);
            var foramtedArea = deliveryArea.PadLeft(7, '0');
            var identification = GetIdentification();
            var formatedDate = receiveDate.ToString("ddMMyy");

            var firstControlDigit = GetFirstControlDigit(receiveDate);
            var secondControlDigit = GetSecondControlDigit(numericCode, receiveDate);

            return $"{countryIsoCode}-{foramtedArea}-{identification}-{formatedDate}-{firstControlDigit}{secondControlDigit}".ToUpper();
        }

        public async Task<short> GetCountryNumericCode(string countryIsoCode)
        {
            var country = (await _unitOfWork.CountryRepository
                .GetAsync(filter: c => c.IsoAplha2Code.ToUpper() == countryIsoCode.ToUpper())).FirstOrDefault();
            var numericCode = country.NumericCode;

            return numericCode;
        }

        public string GetIdentification()
        {
            var guid = Guid.NewGuid().ToString("N");
            var stratIndex = (guid.Length - 1) - 6;
            var identification = guid.Substring(stratIndex, 6);

            return identification;
        }

        public string GetFirstControlDigit(DateTime receiveDate)
        {
            var d_0 = int.Parse(receiveDate.ToString("dd").Substring(0, 1));
            var m_1 = int.Parse(receiveDate.ToString("MM").Substring(1, 1));

            var value = (d_0 + m_1 + 20) * 2;

            return SumUntilOneDigit(value);
        }

        public string GetSecondControlDigit(short numericCode, DateTime receiveDate)
        {
            var y_1 = int.Parse(receiveDate.ToString("yy").Substring(1, 1));

            var value = numericCode + y_1;

            return SumUntilOneDigit(value);
        }

        private string SumUntilOneDigit(int value)
        {
            if (value > 10)
            {
                while (value > 10)
                {
                    var aux = value.ToString();
                    value = aux.ToArray().Sum(c => int.Parse(c.ToString()));
                }
            }

            return value.ToString();
        }

        #endregion

        public List<InsertPackageDTO> ParseXlsFile(IFormFile posteXlsFile)
        {
            var packages = new List<InsertPackageDTO>();

            if(posteXlsFile != null)
            {
                DataSet result = new DataSet();

                using (var stream = posteXlsFile.OpenReadStream())
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        result = reader.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            UseColumnDataType = false,
                            FilterSheet = (tableReader, sheetIndex) =>
                            {
                                if (sheetIndex == 0)
                                {
                                    return true;
                                }
                                return false;
                            }
                        });
                    }
                }

                var colsCount = result.Tables?[0].Columns.Count ?? 0;

                if (colsCount == 12)
                {
                    var rows = result.Tables?[0]?.Rows;

                    if (rows != null)
                    {
                        var rowsEnum = rows.GetEnumerator();

                        while (rowsEnum.MoveNext())
                        {
                            var row = (DataRow)rowsEnum.Current;

                            var rowPackage = new InsertPackageDTO
                            {
                                IsoCountryCode = row[0].ToString(),
                                DeliveryArea = row[1].ToString(),
                                ReceiveDate = DateTime.TryParse(row[2].ToString(), out DateTime colDate) ? colDate : default,
                                PackageValue = decimal.TryParse(row[3].ToString(), out decimal colPckgValue) ? colPckgValue : default,
                                CheckpointId = int.TryParse(row[4].ToString(), out int colChckPointId) ? colChckPointId : default,
                                IsStoppedCustoms = bool.TryParse(row[5].ToString(), out bool colIsStopped) ? colIsStopped : default,
                                HasValueToPay = bool.TryParse(row[6].ToString(), out bool colHasValuePay) ? colHasValuePay : default,
                                PackageWeight = decimal.TryParse(row[7].ToString(), out decimal colPckgWeight) ? colPckgWeight : default,
                                PackageWeightUnitId = byte.TryParse(row[8].ToString(), out byte colWeightUnit) ? colWeightUnit : default,
                                IsFragile = bool.TryParse(row[9].ToString(), out bool colIsFragile) ? colIsFragile : default,
                                PacketSizeId = byte.TryParse(row[10].ToString(), out byte colPacketSize) ? colPacketSize : default                                
                            };

                            packages.Add(rowPackage);
                        }
                    }
                }

            }            
            
            return packages;
        }

        public byte GetReceivedStatusId()
        {
            byte receivedStatusId = byte.Parse(_configuration["TrackerAPISettings:ReceivedStatusId"]);
            return receivedStatusId;
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }

        #endregion
    }
}
