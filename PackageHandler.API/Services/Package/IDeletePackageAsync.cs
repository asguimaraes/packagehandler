﻿using PackageHandler.API.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IDeletePackageAsync : IValidatable
    {
        Task DeletePackageAsync(string trackingCode);
        Task DeletePackageAsync(IEnumerable<string> trackingCodes);
    }
}
