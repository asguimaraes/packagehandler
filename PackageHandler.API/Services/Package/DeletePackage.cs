﻿using Microsoft.Extensions.Configuration;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public class DeletePackage : IDeletePackageAsync
    {
        private IUnitOfWork _unitOfWork;
        private bool _isValid;
        private IDictionary<string, string> _validationErrors;
               

        public DeletePackage(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;            
            _isValid = true;
            _validationErrors = new Dictionary<string, string>();
        }

        #region Methods

        #region Delete

        public async Task DeletePackageAsync(string trackingCode)
        {
            var package = await GetPackageByTrackingCode(trackingCode);

            ValidateDeletion(package);

            if (!_isValid)
            {
                return;
            }

            await DeletePackageOnDb(package);
        }

        public async Task DeletePackageAsync(IEnumerable<string> trackingCodes)
        {
            var packages = new List<Package>();

            var pckgIndex = 0;
            foreach (var trackingCode in trackingCodes)
            {
                if (!_isValid)
                {
                    break;
                }

                var package = await GetPackageByTrackingCode(trackingCode);

                ValidateDeletion(package, pckgIndex);

                if (!_isValid)
                {
                    break; ;
                }

                packages.Add(package);
                pckgIndex++;
            }

            if (!_isValid)
            {
                return;
            }

            await DeletePackageOnDb(packages);
        }

        public async Task DeletePackageOnDb(Package package)
        {
            _unitOfWork.PackageRepository.Delete(package);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeletePackageOnDb(IEnumerable<Package> packages)
        {
            await _unitOfWork.PackageRepository.BulkOperations.DeleteAsync(packages);
        }

        #endregion

        #region Validation

        public void ValidateDeletion(Package package, int? packageIndex = null)
        {
            if (package == null)
            {
                _isValid = false;
                _validationErrors.Add($"Tracking code{(packageIndex != null ? $"[{packageIndex}]" : "")}",
                    "No package found with the provided tracking code");
            }
        }

        #endregion

        public async Task<Package> GetPackageByTrackingCode(string trackingCode)
        {
            return (await _unitOfWork.PackageRepository
                .GetAsync(filter: p => p.TrackingCode.ToUpper() == trackingCode.ToUpper()))
                .FirstOrDefault();
        }

        public IDictionary<string, string> GetValidationErrors()
        {
            return _validationErrors;
        }

        public bool IsValid()
        {
            return _isValid;
        }

        #endregion
    }
}
