﻿using PackageHandler.API.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PackageHandler.API.Services
{
    public interface IUpdatePackageAsync<T> : IValidatable where T : class
    {
        Task UpdatePackageAsync(T package);
        Task UpdatePackageAsync(IEnumerable<T> packages);
    }
}
