using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PackageHandler.API.DAL;
using PackageHandler.API.DTO;
using PackageHandler.API.Services;

namespace PackageHandler.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TrackerContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "PackageHandler.API", Version = "v1" });
            });

            services.AddSingleton(_ => Configuration);

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            
            services.AddScoped<IPackageAsyncValidator, PackageValidator>();
            services.AddScoped<ICheckpointAsyncValidator, CheckpointValidator>();

            services.AddScoped<IGetPackagesAsync<GetPackageDTO>, GetPackages>();
            services.AddScoped<IInsertPackageAsync<InsertPackageDTO>, InsertPackage>();
            services.AddScoped<IUpdatePackageAsync<UpdatePackageDTO>, UpdatePackage>();
            services.AddScoped<IDeletePackageAsync, DeletePackage>();

            services.AddScoped<IGetCheckpointAsync<GetCheckpointDTO>, GetCheckpoints>();
            services.AddScoped<IInsertCheckpointAsync<InsertCheckpointDTO>, InsertCheckpoint>();
            services.AddScoped<IUpdateCheckpointAsync<UpdateCheckpointDTO>, UpdateCheckpoint>();
            services.AddScoped<IDeleteCheckpointAsync, DeleteCheckpoint>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "PackageHandler.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
