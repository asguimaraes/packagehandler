﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PackageHandler.API.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaceTypesController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Get([FromServices] IUnitOfWork unitOfWork)
        {
            try
            {
                var placeTypes = await unitOfWork.PlaceTypeRepository.GetAsync(orderBy: o => o.OrderBy(c => c.Name));
                return Ok(placeTypes);
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
