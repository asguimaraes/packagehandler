﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PackageHandler.API.DTO;
using PackageHandler.API.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PackageHandler.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackagesController : ControllerBase
    {        
        [HttpGet("{trackingCode}")]
        public async Task<IActionResult> Get(string trackingCode, 
            [FromServices] IGetPackagesAsync<GetPackageDTO> getPackagesAsync)
        {
            try
            {
                var packages = await getPackagesAsync.GetPackagesByTrackingCodes(new string[] { trackingCode });

                if (packages.Count() == 0)
                {
                    return BadRequest(new { Message = "No package was found with the informed tracking code" });
                }

                return Ok(new
                {
                    Package = packages.FirstOrDefault()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }
                
        [HttpPost("GetByTrackingCodes")]
        public async Task<IActionResult> GetByTrackingCodes(List<string> trackingCodes, int page = 1, int? pageSize = null, 
            [FromServices]IGetPackagesAsync<GetPackageDTO> getPackagesAsync = default)
        {
            try
            {
                var packages = await getPackagesAsync.GetPackagesByTrackingCodes(trackingCodes, page, pageSize);

                if (packages.Count() == 0)
                {
                    return BadRequest(new { Message = "No packages were found with the informed tracking codes"});
                }

                return Ok(new
                {
                    Packages = packages,
                    NextPage = getPackagesAsync.GetNextPage(),
                    TotalRecords = getPackagesAsync.GetTotalRecords()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpGet("GetByStatus/{statusId}")]
        public async Task<IActionResult> GetByStatus(byte statusId, int page = 1, int? pageSize = null,
            [FromServices] IGetPackagesAsync<GetPackageDTO> getPackagesAsync = default)
        {
            try
            {
                var packages = await getPackagesAsync.GetPackagesByStatus(statusId, page, pageSize);

                if (packages.Count() == 0)
                {
                    return BadRequest(new { Message = "No packages were found with the informed status id" });
                }

                return Ok(new
                {
                    Packages = packages,
                    NextPage = getPackagesAsync.GetNextPage(),
                    TotalRecords = getPackagesAsync.GetTotalRecords()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpGet("GetByCountry/{countryIsoCode}")]
        public async Task<IActionResult> GetByCountry(string countryIsoCode, int page = 1, int? pageSize = null,
            [FromServices] IGetPackagesAsync<GetPackageDTO> getPackagesAsync = default)
        {
            try
            {
                var packages = await getPackagesAsync.GetPackagesByCountry(countryIsoCode, page, pageSize);

                if (packages.Count() == 0)
                {
                    return BadRequest(new { Message = "No packages were found with the informed country iso code" });
                }

                return Ok(new
                {
                    Packages = packages,
                    NextPage = getPackagesAsync.GetNextPage(),
                    TotalRecords = getPackagesAsync.GetTotalRecords()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpGet("GetInTransitValue")]
        public async Task<IActionResult> GetInTransitValue([FromServices] IGetPackagesAsync<GetPackageDTO> getPackagesAsync)
        {
            try
            {
                var inTransitValue = await getPackagesAsync.GetInTransitValue();

                return Ok(new
                {
                    Value = inTransitValue
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPost("Add")]
        public async Task<IActionResult> Add(InsertPackageDTO package, [FromServices]IInsertPackageAsync<InsertPackageDTO> insertPackageAsync)
        {
            try
            {
                var trackingCode = await insertPackageAsync.CreatePackageAsync(package);

                if (!insertPackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = insertPackageAsync.GetValidationErrors()});
                }
                
                return Ok(new { TrackingCode =  trackingCode});
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPost("AddList")]
        public async Task<IActionResult> AddList(List<InsertPackageDTO> packages, [FromServices] IInsertPackageAsync<InsertPackageDTO> insertPackageAsync)
        {
            try
            {
                var trackingCode = await insertPackageAsync.CreatePackageAsync(packages);

                if (!insertPackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = insertPackageAsync.GetValidationErrors() });
                }

                return Ok(new { TrackingCode = trackingCode });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPost("AddFromFile")]
        public async Task<IActionResult> AddFromFile([FromForm] IFormFile xlsFile, [FromServices] IInsertPackageAsync<InsertPackageDTO> insertPackageAsync)
        {
            try
            {
                var trackingCode = await insertPackageAsync.CreatePackagesFromXlsAsync(xlsFile);

                if (!insertPackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = insertPackageAsync.GetValidationErrors() });
                }

                return Ok(new { TrackingCode = trackingCode });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdatePackageDTO package, [FromServices] IUpdatePackageAsync<UpdatePackageDTO> updatePackageAsync)
        {
            try
            {
                await updatePackageAsync.UpdatePackageAsync(package);

                if (!updatePackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = updatePackageAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPut("UpdateList")]
        public async Task<IActionResult> UpdateList(IEnumerable<UpdatePackageDTO> packages, [FromServices] IUpdatePackageAsync<UpdatePackageDTO> updatePackageAsync)
        {
            try
            {
                await updatePackageAsync.UpdatePackageAsync(packages);

                if (!updatePackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = updatePackageAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpDelete("Delete/{trackingCode}")]
        public async Task<IActionResult> Delete(string trackingCode, [FromServices] IDeletePackageAsync deletePackageAsync)
        {
            try
            {
                await deletePackageAsync.DeletePackageAsync(trackingCode);

                if (!deletePackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = deletePackageAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpDelete("DeleteList")]
        public async Task<IActionResult> DeleteList(List<string> trackingCodes, [FromServices] IDeletePackageAsync deletePackageAsync)
        {
            try
            {
                await deletePackageAsync.DeletePackageAsync(trackingCodes);

                if (!deletePackageAsync.IsValid())
                {
                    return BadRequest(new { Errors = deletePackageAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

    }
}
