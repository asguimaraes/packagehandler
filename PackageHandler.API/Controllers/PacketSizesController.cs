﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PackageHandler.API.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacketSizesController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Get([FromServices] IUnitOfWork unitOfWork)
        {
            try
            {
                var packetSizes = await unitOfWork.PacketSizeRepository.GetAsync(orderBy: o => o.OrderBy(c => c.Name));
                return Ok(packetSizes);
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
