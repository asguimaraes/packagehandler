﻿using Microsoft.AspNetCore.Mvc;
using PackageHandler.API.DTO;
using PackageHandler.API.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PackageHandler.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckpointsController : ControllerBase
    {
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id,
            [FromServices] IGetCheckpointAsync<GetCheckpointDTO> getCheckpointsAsync)
        {
            try
            {
                var checkpoints = await getCheckpointsAsync.GetCheckpointsById(new int[] { id });

                if (checkpoints.Count() == 0)
                {
                    return BadRequest(new { Message = "No checkpoint was found with the informed id" });
                }

                return Ok(new
                {
                    Checkpoint = checkpoints.FirstOrDefault()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPost("GetByIds")]
        public async Task<IActionResult> GetByIds(List<int> ids, int page = 1, int? pageSize = null,
            [FromServices] IGetCheckpointAsync<GetCheckpointDTO> getCheckpointsAsync = default)
        {
            try
            {
                var checkpoints = await getCheckpointsAsync.GetCheckpointsById(ids, page, pageSize);

                if (checkpoints.Count() == 0)
                {
                    return BadRequest(new { Message = "No checpoints were found with the informed ids" });
                }

                return Ok(new
                {
                    Packages = checkpoints,
                    NextPage = getCheckpointsAsync.GetNextPage(),
                    TotalRecords = getCheckpointsAsync.GetTotalRecords()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }


        [HttpGet("GetByCountry/{countryIsoCode}")]
        public async Task<IActionResult> GetByCountry(string countryIsoCode, int page = 1, int? pageSize = null,
            [FromServices] IGetCheckpointAsync<GetCheckpointDTO> getCheckpointsAsync = default)
        {
            try
            {
                var checkpoints = await getCheckpointsAsync.GetCheckpointsByCountry(countryIsoCode, page, pageSize);

                if (checkpoints.Count() == 0)
                {
                    return BadRequest(new { Message = "No checkpoints were found with the informed country iso code" });
                }

                return Ok(new
                {
                    Packages = checkpoints,
                    NextPage = getCheckpointsAsync.GetNextPage(),
                    TotalRecords = getCheckpointsAsync.GetTotalRecords()
                });
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPost("Add")]
        public async Task<IActionResult> Add(InsertCheckpointDTO checkpoint, 
            [FromServices] IInsertCheckpointAsync<InsertCheckpointDTO> insertCheckpointAsync)
        {
            try
            {
                await insertCheckpointAsync.CreateCheckpointAsync(checkpoint);

                if (!insertCheckpointAsync.IsValid())
                {
                    return BadRequest(new { Errors = insertCheckpointAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdateCheckpointDTO checkpoint, 
            [FromServices] IUpdateCheckpointAsync<UpdateCheckpointDTO> updateCheckpointAsync)
        {
            try
            {
                await updateCheckpointAsync.UpdateCheckpointAsync(checkpoint);

                if (!updateCheckpointAsync.IsValid())
                {
                    return BadRequest(new { Errors = updateCheckpointAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id, [FromServices] IDeleteCheckpointAsync deleteCheckpointAsync)
        {
            try
            {
                await deleteCheckpointAsync.DeleteCheckpointAsync(id);

                if (!deleteCheckpointAsync.IsValid())
                {
                    return BadRequest(new { Errors = deleteCheckpointAsync.GetValidationErrors() });
                }

                return Ok();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
