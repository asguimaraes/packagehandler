USE [db_tracker]
GO
/****** Object:  UserDefinedTableType [dbo].[PackageTableType]    Script Date: 6/28/2021 5:14:59 AM ******/
CREATE TYPE [dbo].[PackageTableType] AS TABLE(
	[Id] [bigint] NULL,
	[TrackingCode] [nvarchar](27) NOT NULL,
	[ReceiveDate] [datetime] NOT NULL,
	[PackageValue] [numeric](11, 2) NULL,
	[IsStoppedCustoms] [bit] NOT NULL,
	[HasValueToPay] [bit] NOT NULL,
	[Weight] [numeric](7, 3) NOT NULL,
	[IsFragile] [bit] NOT NULL,
	[FkCheckpointId] [int] NOT NULL,
	[FkWeightUnitId] [tinyint] NOT NULL,
	[FkPacketSizeId] [tinyint] NOT NULL,
	[FkStatusId] [tinyint] NOT NULL
)
GO
/****** Object:  Table [dbo].[Checkpoints]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Checkpoints](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[City] [nvarchar](100) NOT NULL,
	[FkCountryId] [smallint] NOT NULL,
	[FkControlTypeId] [tinyint] NOT NULL,
	[FkPlaceTypeId] [tinyint] NOT NULL,
 CONSTRAINT [PK_Checkpoints] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ControlTypes]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControlTypes](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_ControlTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[Id] [smallint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[NumericCode] [smallint] NOT NULL,
	[IsoAplha2Code] [nvarchar](2) NOT NULL,
	[IsoAplha3Code] [nvarchar](3) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Packages]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Packages](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TrackingCode] [nvarchar](27) NOT NULL,
	[ReceiveDate] [datetime] NOT NULL,
	[PackageValue] [numeric](11, 2) NULL,
	[IsStoppedCustoms] [bit] NOT NULL,
	[HasValueToPay] [bit] NOT NULL,
	[Weight] [numeric](7, 3) NOT NULL,
	[IsFragile] [bit] NOT NULL,
	[FkCheckpointId] [int] NOT NULL,
	[FkWeightUnitId] [tinyint] NOT NULL,
	[FkPacketSizeId] [tinyint] NOT NULL,
	[FkStatusId] [tinyint] NOT NULL,
 CONSTRAINT [PK_Packages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PackageStatuses]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageStatuses](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](25) NULL,
 CONSTRAINT [PK_PackageStatuses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PacketSizes]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PacketSizes](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](4) NULL,
 CONSTRAINT [PK_PacketSizes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlaceTypes]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaceTypes](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_PlaceTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WeightUnits]    Script Date: 6/28/2021 5:14:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WeightUnits](
	[Id] [tinyint] NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Abbreviation] [nvarchar](6) NULL,
 CONSTRAINT [PK_WeightUnits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ControlTypes] ([Id], [Name]) VALUES (1, N'Passage')
GO
INSERT [dbo].[ControlTypes] ([Id], [Name]) VALUES (2, N'Customs')
GO
INSERT [dbo].[ControlTypes] ([Id], [Name]) VALUES (3, N'Final Check')
GO
SET IDENTITY_INSERT [dbo].[Countries] ON 
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (1, N'Afghanistan', 4, N'AF', N'AFG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (2, N'Åland Islands', 248, N'AX', N'ALA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (3, N'Albania', 8, N'AL', N'ALB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (4, N'Algeria', 12, N'DZ', N'DZA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (5, N'American Samoa', 16, N'AS', N'ASM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (6, N'Andorra', 20, N'AD', N'AND')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (7, N'Angola', 24, N'AO', N'AGO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (8, N'Anguilla', 660, N'AI', N'AIA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (9, N'Antarctica', 10, N'AQ', N'ATA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (10, N'Antigua and Barbuda', 28, N'AG', N'ATG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (11, N'Argentina', 32, N'AR', N'ARG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (12, N'Armenia', 51, N'AM', N'ARM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (13, N'Aruba', 533, N'AW', N'ABW')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (14, N'Australia', 36, N'AU', N'AUS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (15, N'Austria', 40, N'AT', N'AUT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (16, N'Azerbaijan', 31, N'AZ', N'AZE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (17, N'Bahamas', 44, N'BS', N'BHS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (18, N'Bahrain', 48, N'BH', N'BHR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (19, N'Bangladesh', 50, N'BD', N'BGD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (20, N'Barbados', 52, N'BB', N'BRB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (21, N'Belarus', 112, N'BY', N'BLR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (22, N'Belgium', 56, N'BE', N'BEL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (23, N'Belize', 84, N'BZ', N'BLZ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (24, N'Benin', 204, N'BJ', N'BEN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (25, N'Bermuda', 60, N'BM', N'BMU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (26, N'Bhutan', 64, N'BT', N'BTN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (27, N'Bolivia', 68, N'BO', N'BOL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (28, N'Bonaire, Sint Eustatius, and Saba', 535, N'BQ', N'BES')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (29, N'Bosnia and Herzegovina', 70, N'BA', N'BIH')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (30, N'Botswana', 72, N'BW', N'BWA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (31, N'Bouvet Island', 74, N'BV', N'BVT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (32, N'Brazil', 76, N'BR', N'BRA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (33, N'British Indian Ocean Territory', 86, N'IO', N'IOT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (34, N'Brunei Darussalam', 96, N'BN', N'BRN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (35, N'Bulgaria', 100, N'BG', N'BGR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (36, N'Burkina Faso', 854, N'BF', N'BFA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (37, N'Burundi', 108, N'BI', N'BDI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (38, N'Cabo Verde', 132, N'CV', N'CPV')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (39, N'Cambodia', 116, N'KH', N'KHM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (40, N'Cameroon', 120, N'CM', N'CMR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (41, N'Canada', 124, N'CA', N'CAN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (42, N'Cayman Islands', 136, N'KY', N'CYM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (43, N'Central African Republic', 140, N'CF', N'CAF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (44, N'Chad', 148, N'TD', N'TCD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (45, N'Chile', 152, N'CL', N'CHL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (46, N'China', 156, N'CN', N'CHN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (47, N'Christmas Island', 162, N'CX', N'CXR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (48, N'Cocos (Keeling) Islands', 166, N'CC', N'CCK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (49, N'Colombia', 170, N'CO', N'COL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (50, N'Comoros', 174, N'KM', N'COM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (51, N'Congo (the Democratic Republic of the Congo)', 180, N'CD', N'COD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (52, N'Congo', 178, N'CG', N'COG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (53, N'Cook Islands', 184, N'CK', N'COK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (54, N'Costa Rica', 188, N'CR', N'CRI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (55, N'Côte d''Ivoire', 384, N'CI', N'CIV')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (56, N'Croatia', 191, N'HR', N'HRV')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (57, N'Cuba', 192, N'CU', N'CUB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (58, N'Curaçao', 531, N'CW', N'CUW')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (59, N'Cyprus', 196, N'CY', N'CYP')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (60, N'Czechia', 203, N'CZ', N'CZE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (61, N'Denmark', 208, N'DK', N'DNK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (62, N'Djibouti', 262, N'DJ', N'DJI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (63, N'Dominica', 212, N'DM', N'DMA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (64, N'Dominican Republic', 214, N'DO', N'DOM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (65, N'Ecuador', 218, N'EC', N'ECU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (66, N'Egypt', 818, N'EG', N'EGY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (67, N'El Salvador', 222, N'SV', N'SLV')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (68, N'Equatorial Guinea', 226, N'GQ', N'GNQ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (69, N'Eritrea', 232, N'ER', N'ERI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (70, N'Estonia', 233, N'EE', N'EST')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (71, N'Eswatini', 748, N'SZ', N'SWZ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (72, N'Ethiopia', 231, N'ET', N'ETH')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (73, N'Falkland Islands', 238, N'FK', N'FLK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (74, N'Faroe Islands', 234, N'FO', N'FRO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (75, N'Fiji', 242, N'FJ', N'FJI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (76, N'Finland', 246, N'FI', N'FIN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (77, N'France', 250, N'FR', N'FRA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (78, N'French Guiana', 254, N'GF', N'GUF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (79, N'French Polynesia', 258, N'PF', N'PYF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (80, N'French Southern Territories', 260, N'TF', N'ATF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (81, N'Gabon', 266, N'GA', N'GAB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (82, N'Gambia', 270, N'GM', N'GMB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (83, N'Georgia', 268, N'GE', N'GEO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (84, N'Germany', 276, N'DE', N'DEU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (85, N'Ghana', 288, N'GH', N'GHA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (86, N'Gibraltar', 292, N'GI', N'GIB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (87, N'Greece', 300, N'GR', N'GRC')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (88, N'Greenland', 304, N'GL', N'GRL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (89, N'Grenada', 308, N'GD', N'GRD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (90, N'Guadeloupe', 312, N'GP', N'GLP')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (91, N'Guam', 316, N'GU', N'GUM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (92, N'Guatemala', 320, N'GT', N'GTM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (93, N'Guernsey', 831, N'GG', N'GGY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (94, N'Guinea-Bissau', 624, N'GW', N'GNB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (95, N'Guinea', 324, N'GN', N'GIN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (96, N'Guyana', 328, N'GY', N'GUY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (97, N'Haiti', 332, N'HT', N'HTI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (98, N'Heard Island and McDonald Islands', 334, N'HM', N'HMD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (99, N'Holy See', 336, N'VA', N'VAT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (100, N'Honduras', 340, N'HN', N'HND')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (101, N'Hong Kong', 344, N'HK', N'HKG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (102, N'Hungary', 348, N'HU', N'HUN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (103, N'Iceland', 352, N'IS', N'ISL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (104, N'India', 356, N'IN', N'IND')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (105, N'Indonesia', 360, N'ID', N'IDN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (106, N'Iran', 364, N'IR', N'IRN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (107, N'Iraq', 368, N'IQ', N'IRQ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (108, N'Ireland', 372, N'IE', N'IRL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (109, N'Isle of Man', 833, N'IM', N'IMN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (110, N'Israel', 376, N'IL', N'ISR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (111, N'Italy', 380, N'IT', N'ITA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (112, N'Jamaica', 388, N'JM', N'JAM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (113, N'Japan', 392, N'JP', N'JPN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (114, N'Jersey', 832, N'JE', N'JEY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (115, N'Jordan', 400, N'JO', N'JOR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (116, N'Kazakhstan', 398, N'KZ', N'KAZ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (117, N'Kenya', 404, N'KE', N'KEN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (118, N'Kiribati', 296, N'KI', N'KIR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (119, N'Korea (the Democratic People''s Republic of Korea)', 408, N'KP', N'PRK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (120, N'Korea (the Republic of Korea)', 410, N'KR', N'KOR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (121, N'Kuwait', 414, N'KW', N'KWT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (122, N'Kyrgyzstan', 417, N'KG', N'KGZ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (123, N'Lao People''s Democratic Republic', 418, N'LA', N'LAO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (124, N'Latvia', 428, N'LV', N'LVA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (125, N'Lebanon', 422, N'LB', N'LBN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (126, N'Lesotho', 426, N'LS', N'LSO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (127, N'Liberia', 430, N'LR', N'LBR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (128, N'Libya', 434, N'LY', N'LBY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (129, N'Liechtenstein', 438, N'LI', N'LIE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (130, N'Lithuania', 440, N'LT', N'LTU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (131, N'Luxembourg', 442, N'LU', N'LUX')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (132, N'Macao', 446, N'MO', N'MAC')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (133, N'Macedonia', 807, N'MK', N'MKD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (134, N'Madagascar', 450, N'MG', N'MDG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (135, N'Malawi', 454, N'MW', N'MWI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (136, N'Malaysia', 458, N'MY', N'MYS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (137, N'Maldives', 462, N'MV', N'MDV')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (138, N'Mali', 466, N'ML', N'MLI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (139, N'Malta', 470, N'MT', N'MLT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (140, N'Marshall Islands', 584, N'MH', N'MHL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (141, N'Martinique', 474, N'MQ', N'MTQ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (142, N'Mauritania', 478, N'MR', N'MRT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (143, N'Mauritius', 480, N'MU', N'MUS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (144, N'Mayotte', 175, N'YT', N'MYT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (145, N'Mexico', 484, N'MX', N'MEX')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (146, N'Micronesia', 583, N'FM', N'FSM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (147, N'Moldova', 498, N'MD', N'MDA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (148, N'Monaco', 492, N'MC', N'MCO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (149, N'Mongolia', 496, N'MN', N'MNG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (150, N'Montenegro', 499, N'ME', N'MNE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (151, N'Montserrat', 500, N'MS', N'MSR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (152, N'Morocco', 504, N'MA', N'MAR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (153, N'Mozambique', 508, N'MZ', N'MOZ')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (154, N'Myanmar', 104, N'MM', N'MMR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (155, N'Namibia', 516, N'NA', N'NAM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (156, N'Nauru', 520, N'NR', N'NRU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (157, N'Nepal', 524, N'NP', N'NPL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (158, N'Netherlands', 528, N'NL', N'NLD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (159, N'New Caledonia', 540, N'NC', N'NCL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (160, N'New Zealand', 554, N'NZ', N'NZL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (161, N'Nicaragua', 558, N'NI', N'NIC')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (162, N'Niger', 562, N'NE', N'NER')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (163, N'Nigeria', 566, N'NG', N'NGA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (164, N'Niue', 570, N'NU', N'NIU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (165, N'Norfolk Island', 574, N'NF', N'NFK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (166, N'Northern Mariana Islands', 580, N'MP', N'MNP')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (167, N'Norway', 578, N'NO', N'NOR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (168, N'Oman', 512, N'OM', N'OMN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (169, N'Pakistan', 586, N'PK', N'PAK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (170, N'Palau', 585, N'PW', N'PLW')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (171, N'Palestine', 275, N'PS', N'PSE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (172, N'Panama', 591, N'PA', N'PAN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (173, N'Papua New Guinea', 598, N'PG', N'PNG')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (174, N'Paraguay', 600, N'PY', N'PRY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (175, N'Peru', 604, N'PE', N'PER')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (176, N'Philippines', 608, N'PH', N'PHL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (177, N'Pitcairn', 612, N'PN', N'PCN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (178, N'Poland', 616, N'PL', N'POL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (179, N'Portugal', 620, N'PT', N'PRT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (180, N'Puerto Rico', 630, N'PR', N'PRI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (181, N'Qatar', 634, N'QA', N'QAT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (182, N'Réunion', 638, N'RE', N'REU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (183, N'Romania', 642, N'RO', N'ROU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (184, N'Russian Federation', 643, N'RU', N'RUS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (185, N'Rwanda', 646, N'RW', N'RWA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (186, N'Saint Barthélemy', 652, N'BL', N'BLM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (187, N'Saint Helena, Ascension and Tristan da Cunha', 654, N'SH', N'SHN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (188, N'Saint Kitts and Nevis', 659, N'KN', N'KNA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (189, N'Saint Lucia', 662, N'LC', N'LCA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (190, N'Saint Martin (French part)', 663, N'MF', N'MAF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (191, N'Saint Pierre and Miquelon', 666, N'PM', N'SPM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (192, N'Saint Vincent and the Grenadines', 670, N'VC', N'VCT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (193, N'Samoa', 882, N'WS', N'WSM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (194, N'San Marino', 674, N'SM', N'SMR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (195, N'Sao Tome and Principe', 678, N'ST', N'STP')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (196, N'Saudi Arabia', 682, N'SA', N'SAU')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (197, N'Senegal', 686, N'SN', N'SEN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (198, N'Serbia', 688, N'RS', N'SRB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (199, N'Seychelles', 690, N'SC', N'SYC')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (200, N'Sierra Leone', 694, N'SL', N'SLE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (201, N'Singapore', 702, N'SG', N'SGP')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (202, N'Sint Maarten (Dutch part)', 534, N'SX', N'SXM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (203, N'Slovakia', 703, N'SK', N'SVK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (204, N'Slovenia', 705, N'SI', N'SVN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (205, N'Solomon Islands', 90, N'SB', N'SLB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (206, N'Somalia', 706, N'SO', N'SOM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (207, N'South Africa', 710, N'ZA', N'ZAF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (208, N'South Georgia and the South Sandwich Islands', 239, N'GS', N'SGS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (209, N'South Sudan', 728, N'SS', N'SSD')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (210, N'Spain', 724, N'ES', N'ESP')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (211, N'Sri Lanka', 144, N'LK', N'LKA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (212, N'Sudan', 729, N'SD', N'SDN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (213, N'Suriname', 740, N'SR', N'SUR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (214, N'Svalbard and Jan Mayen', 744, N'SJ', N'SJM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (215, N'Sweden', 752, N'SE', N'SWE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (216, N'Switzerland', 756, N'CH', N'CHE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (217, N'Syrian Arab Republic', 760, N'SY', N'SYR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (218, N'Taiwan', 158, N'TW', N'TWN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (219, N'Tajikistan', 762, N'TJ', N'TJK')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (220, N'Tanzania', 834, N'TZ', N'TZA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (221, N'Thailand', 764, N'TH', N'THA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (222, N'Timor-Leste', 626, N'TL', N'TLS')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (223, N'Togo', 768, N'TG', N'TGO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (224, N'Tokelau', 772, N'TK', N'TKL')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (225, N'Tonga', 776, N'TO', N'TON')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (226, N'Trinidad and Tobago', 780, N'TT', N'TTO')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (227, N'Tunisia', 788, N'TN', N'TUN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (228, N'Turkey', 792, N'TR', N'TUR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (229, N'Turkmenistan', 795, N'TM', N'TKM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (230, N'Turks and Caicos Islands', 796, N'TC', N'TCA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (231, N'Tuvalu', 798, N'TV', N'TUV')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (232, N'Uganda', 800, N'UG', N'UGA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (233, N'Ukraine', 804, N'UA', N'UKR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (234, N'United Arab Emirates', 784, N'AE', N'ARE')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (235, N'United Kingdom of Great Britain and Northern Ireland', 826, N'GB', N'GBR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (236, N'United States Minor Outlying Islands', 581, N'UM', N'UMI')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (237, N'United States of America', 840, N'US', N'USA')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (238, N'Uruguay', 858, N'UY', N'URY')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (239, N'Uzbekistan', 860, N'UZ', N'UZB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (240, N'Vanuatu', 548, N'VU', N'VUT')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (241, N'Venezuela', 862, N'VE', N'VEN')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (242, N'Viet Nam', 704, N'VN', N'VNM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (243, N'Virgin Islands (British)', 92, N'VG', N'VGB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (244, N'Virgin Islands (U.S.)', 850, N'VI', N'VIR')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (245, N'Wallis and Futuna', 876, N'WF', N'WLF')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (246, N'Western Sahara', 732, N'EH', N'ESH')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (247, N'Yemen', 887, N'YE', N'YEM')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (248, N'Zambia', 894, N'ZM', N'ZMB')
GO
INSERT [dbo].[Countries] ([Id], [Name], [NumericCode], [IsoAplha2Code], [IsoAplha3Code]) VALUES (249, N'Zimbabwe', 716, N'ZW', N'ZWE')
GO
SET IDENTITY_INSERT [dbo].[Countries] OFF
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (1, N'Received')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (2, N' In transit')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (3, N'Stopped by legal')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (4, N'Attemped delivery')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (5, N'Returning')
GO
INSERT [dbo].[PackageStatuses] ([Id], [Name]) VALUES (6, N'Delivered')
GO
INSERT [dbo].[PacketSizes] ([Id], [Name]) VALUES (1, N'XS')
GO
INSERT [dbo].[PacketSizes] ([Id], [Name]) VALUES (2, N'S')
GO
INSERT [dbo].[PacketSizes] ([Id], [Name]) VALUES (3, N'M')
GO
INSERT [dbo].[PacketSizes] ([Id], [Name]) VALUES (4, N'L')
GO
INSERT [dbo].[PacketSizes] ([Id], [Name]) VALUES (5, N'XL')
GO
INSERT [dbo].[PacketSizes] ([Id], [Name]) VALUES (6, N'XXL')
GO
INSERT [dbo].[PlaceTypes] ([Id], [Name]) VALUES (1, N'Airport')
GO
INSERT [dbo].[PlaceTypes] ([Id], [Name]) VALUES (2, N'Port')
GO
INSERT [dbo].[PlaceTypes] ([Id], [Name]) VALUES (3, N'Station')
GO
INSERT [dbo].[PlaceTypes] ([Id], [Name]) VALUES (4, N'Customs Facility')
GO
INSERT [dbo].[PlaceTypes] ([Id], [Name]) VALUES (5, N'External')
GO
INSERT [dbo].[WeightUnits] ([Id], [Name], [Abbreviation]) VALUES (1, N'gram', N'g')
GO
INSERT [dbo].[WeightUnits] ([Id], [Name], [Abbreviation]) VALUES (2, N'kilogram', N'kg')
GO
ALTER TABLE [dbo].[Checkpoints] ADD  DEFAULT (CONVERT([smallint],(0))) FOR [FkCountryId]
GO
ALTER TABLE [dbo].[Checkpoints] ADD  DEFAULT (CONVERT([tinyint],(0))) FOR [FkControlTypeId]
GO
ALTER TABLE [dbo].[Checkpoints] ADD  DEFAULT (CONVERT([tinyint],(0))) FOR [FkPlaceTypeId]
GO
ALTER TABLE [dbo].[Packages] ADD  DEFAULT ((0)) FOR [FkCheckpointId]
GO
ALTER TABLE [dbo].[Packages] ADD  DEFAULT (CONVERT([tinyint],(0))) FOR [FkWeightUnitId]
GO
ALTER TABLE [dbo].[Packages] ADD  DEFAULT (CONVERT([tinyint],(0))) FOR [FkPacketSizeId]
GO
ALTER TABLE [dbo].[Packages] ADD  DEFAULT (CONVERT([tinyint],(0))) FOR [FkStatusId]
GO
ALTER TABLE [dbo].[Checkpoints]  WITH CHECK ADD  CONSTRAINT [FK_Checkpoints_ControlTypes_FkControlTypeId] FOREIGN KEY([FkControlTypeId])
REFERENCES [dbo].[ControlTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Checkpoints] CHECK CONSTRAINT [FK_Checkpoints_ControlTypes_FkControlTypeId]
GO
ALTER TABLE [dbo].[Checkpoints]  WITH CHECK ADD  CONSTRAINT [FK_Checkpoints_Countries_FkCountryId] FOREIGN KEY([FkCountryId])
REFERENCES [dbo].[Countries] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Checkpoints] CHECK CONSTRAINT [FK_Checkpoints_Countries_FkCountryId]
GO
ALTER TABLE [dbo].[Checkpoints]  WITH CHECK ADD  CONSTRAINT [FK_Checkpoints_PlaceTypes_FkPlaceTypeId] FOREIGN KEY([FkPlaceTypeId])
REFERENCES [dbo].[PlaceTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Checkpoints] CHECK CONSTRAINT [FK_Checkpoints_PlaceTypes_FkPlaceTypeId]
GO
ALTER TABLE [dbo].[Packages]  WITH CHECK ADD  CONSTRAINT [FK_Packages_Checkpoints_FkCheckpointId] FOREIGN KEY([FkCheckpointId])
REFERENCES [dbo].[Checkpoints] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Packages] CHECK CONSTRAINT [FK_Packages_Checkpoints_FkCheckpointId]
GO
ALTER TABLE [dbo].[Packages]  WITH CHECK ADD  CONSTRAINT [FK_Packages_PackageStatuses_FkStatusId] FOREIGN KEY([FkStatusId])
REFERENCES [dbo].[PackageStatuses] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Packages] CHECK CONSTRAINT [FK_Packages_PackageStatuses_FkStatusId]
GO
ALTER TABLE [dbo].[Packages]  WITH CHECK ADD  CONSTRAINT [FK_Packages_PacketSizes_FkPacketSizeId] FOREIGN KEY([FkPacketSizeId])
REFERENCES [dbo].[PacketSizes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Packages] CHECK CONSTRAINT [FK_Packages_PacketSizes_FkPacketSizeId]
GO
ALTER TABLE [dbo].[Packages]  WITH CHECK ADD  CONSTRAINT [FK_Packages_WeightUnits_FkWeightUnitId] FOREIGN KEY([FkWeightUnitId])
REFERENCES [dbo].[WeightUnits] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Packages] CHECK CONSTRAINT [FK_Packages_WeightUnits_FkWeightUnitId]
GO
/****** Object:  StoredProcedure [dbo].[SpDeletePackages]    Script Date: 6/28/2021 5:15:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SpDeletePackages] 
	(@packagesToDelete dbo.PackageTableType READONLY)
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE [dbo].[Packages] FROM [dbo].[Packages] 
	INNER JOIN @packagesToDelete as up
	ON [dbo].[Packages].[Id] = up.Id

END
GO
/****** Object:  StoredProcedure [dbo].[SpInsertPackages]    Script Date: 6/28/2021 5:15:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SpInsertPackages] 
	(@newPackages dbo.PackageTableType READONLY)
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Packages]
           ([TrackingCode]
           ,[ReceiveDate]
           ,[PackageValue]
           ,[IsStoppedCustoms]
           ,[HasValueToPay]
           ,[Weight]
           ,[IsFragile]
           ,[FkCheckpointId]
           ,[FkWeightUnitId]
           ,[FkPacketSizeId]
           ,[FkStatusId])
		SELECT np.TrackingCode,
           np.ReceiveDate,
           np.PackageValue,
           np.IsStoppedCustoms,
           np.HasValueToPay,
           np.[Weight],
           np.IsFragile,
           np.FkCheckpointId,
           np.FkWeightUnitId,
           np.FkPacketSizeId,
           np.FkStatusId
		FROM @newPackages np

END
GO
/****** Object:  StoredProcedure [dbo].[SpUpdatePackages]    Script Date: 6/28/2021 5:15:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpUpdatePackages] 
	(@updatedPackages dbo.PackageTableType READONLY)
AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE [dbo].[Packages]
	  SET [TrackingCode] = up.TrackingCode,
	      [ReceiveDate] = up.ReceiveDate,
	      [PackageValue] = up.PackageValue,
	      [IsStoppedCustoms] = up.IsStoppedCustoms,
	      [HasValueToPay] = up.HasValueToPay,
	      [Weight] = up.[Weight],
	      [IsFragile] = up.IsFragile,
	      [FkCheckpointId] = up.FkCheckpointId,
	      [FkWeightUnitId] = up.FkWeightUnitId,
	      [FkPacketSizeId] = up.FkPacketSizeId,
	      [FkStatusId] = up.FkStatusId
	FROM [dbo].[Packages] INNER JOIN @updatedPackages as up
	ON [dbo].[Packages].[Id] = up.Id

END
GO
