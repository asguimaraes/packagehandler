# Challenge #

Create a package tracking API

### Requirements ###
* .NET 5
* Microsoft SQL Server

### Instalation ###

#### Database creation ####

Run the scripts on SQL Scripts folder in this order:

* CREATE DATABASE.sql
* CREATE TABLES.sql

Attention for the database files path in the first script, they should be changed.

### Usage ###

- Change the connection string present on appsettings.json to use your local SQL Server instance.
- Run PackageHandler.API project to start the application.
- Swagger page was not edited and for that i'm sorry. But with what was automatically generate we can have a general understanding. Just wanna point some items:
	- Paged results:
		- Some Packages actions return paginated results, page default size is set to 500 on appsettings file.
		- Every paged result returns: the result set, total records from the query and the next page number.
		- When the next page won't return results it will be set to -1.
	- Package insertion with .xls file:
		- **File must not have headers and columns should follow this order:**
		- *Iso country code*
		- *Delivery area code*
		- *Receive date*
		- *Package value*
		- *Last checkpoint Id*
		- *Stopped on customs flag*
		- *Has value to pay flag*
		- *Package weight*
		- *Package weight unit id*
		- *Fragile flag*
		- *Packet size id*

### Testing ###
- Some test have the connection string hardcoded in the classes, please change it to use your test database.

