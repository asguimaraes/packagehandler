using System;
using Xunit;
using PackageHandler.API.DAL;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Threading.Tasks;
using PackageHandler.API.DAL.Entities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace PackageHandler.API.Tests
{
    public class DALTest
    {
        private Mock<IConfiguration> GetConfigurationMock()
        {
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "DefaultConnection")])
                .Returns(@"data source=DESKTOP-DIR21QH\SQLEXPRESS2;Integrated Security=SSPI;initial catalog=db_tracker;Max Pool Size=1000;");

            var mockConfig = new Mock<IConfiguration>();
            mockConfig.Setup(c => c.GetSection(It.Is<string>(s => s == "ConnectionStrings")))
                .Returns(mockConfSection.Object);

            return mockConfig;
        }
        
        private TrackerContext GetContext()
        {
            var configuration = GetConfigurationMock().Object;
            var optionsBuilder = new DbContextOptionsBuilder<TrackerContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            var context = new TrackerContext(optionsBuilder.Options);

            return context;
        }

        private List<Package> GetPackageTestData(int length)
        {
            var packageList = new List<Package>();

            for(int i = 0; i < length; i++)
            {
                var package = new Package
                {
                    TrackingCode = Guid.NewGuid().ToString().Substring(0,27),   
                    ReceiveDate = DateTime.UtcNow,
                    Weight = 2,
                    FkCheckpointId = 1,
                    FkWeightUnitId = 1,
                    FkPacketSizeId = 1,
                    FkStatusId = 1
                };
                packageList.Add(package);
            }

            return packageList;
        }
                
        #region Country

        [Fact]
        public void GetAllCountries_Test()
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var countryRepository = unitOfWork.CountryRepository;

            var countries = countryRepository.Get();

            Assert.True(countries.Count() > 0);
        }

        [Fact]
        public async Task GettAllCountriesAsync_Test()
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var countryRepository = unitOfWork.CountryRepository;

            var countries = await countryRepository.GetAsync();

            Assert.True(countries.Count() > 0);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(248)]
        [InlineData(76)]
        public void GetCountryByNumericCode_Test(short value)
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var countryRepository = unitOfWork.CountryRepository;

            var countries = countryRepository.Get(filter: x => x.NumericCode == value);

            Assert.True(countries.Count() > 0);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(248)]
        [InlineData(76)]
        public async Task GetCountryByNumericCodeAsync_Test(short value)
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var countryRepository = unitOfWork.CountryRepository;

            var countries = await countryRepository.GetAsync(filter: x => x.NumericCode == value);

            Assert.True(countries.Count() > 0);
        }

        [Theory]
        [InlineData("BE")]
        [InlineData("PT")]
        [InlineData("BR")]
        public void GetCountryByISOCode_Test(string value)
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var countryRepository = unitOfWork.CountryRepository;

            var countries = countryRepository.Get(filter: x => x.IsoAplha2Code == value);

            Assert.True(countries.Count() > 0);
        }

        [Theory]
        [InlineData("BE")]
        [InlineData("PT")]
        [InlineData("BR")]
        public async Task GetCountryByISOCodeAsync_Test(string value)
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var countryRepository = unitOfWork.CountryRepository;

            var countries = await countryRepository.GetAsync(filter: x => x.IsoAplha2Code == value);

            Assert.True(countries.Count() > 0);
        }

        #endregion

        #region Checkpoint
        
        [Fact]
        public void InsertCheckpoint_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var checkpointRepository = unitOfWork.CheckpointRepository;

            checkpointRepository.Insert(new Checkpoint { 
                Name = "TEST CHECKPOINT 1",
                City = "S�o Lu�s",                
                FkCountryId = 32,
                FkControlTypeId = 1,
                FkPlaceTypeId = 1
            });

            unitOfWork.Save();
        }

        [Fact]
        public async Task InsertCheckpointAsync_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var checkpointRepository = unitOfWork.CheckpointRepository;

            await checkpointRepository.InsertAsync(new Checkpoint
            {
                Name = "TEST CHECKPOINT 2",
                City = "S�o Lu�s",
                FkCountryId = 32,
                FkControlTypeId = 1,
                FkPlaceTypeId = 1
            });

            unitOfWork.Save();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void GetCheckpointById_Test(int value)
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var checkpointRepository = unitOfWork.CheckpointRepository;

            var checkpoint = checkpointRepository.GetByID(value);

            Assert.NotNull(checkpoint);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task GetCheckpointByIdAsync_Test(int value)
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var checkpointRepository = unitOfWork.CheckpointRepository;

            var checkpoint = await checkpointRepository.GetByIDAsync(value);

            Assert.NotNull(checkpoint);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void UpdateCheckpoint_Test(int value)
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var checkpointRepository = unitOfWork.CheckpointRepository;

            var checkpoint = checkpointRepository.GetByID(value);

            checkpoint.Name = $"{checkpoint.Name} SYNC";
            checkpointRepository.Update(checkpoint);

            unitOfWork.Save();
        }
                
        #endregion

        #region Package
        
        [Fact]
        public void InsertPackageBulk_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            packageRepository.BulkOperations = new PackageBulkOperations(context);

            packageRepository.BulkOperations.Insert(GetPackageTestData(10000));
        }

        [Fact]
        public async Task InsertPackageBulkAsync_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            packageRepository.BulkOperations = new PackageBulkOperations(context);

            await packageRepository.BulkOperations.InsertAsync(GetPackageTestData(10000));
        }

        [Fact]
        public void GetAllPackages_Test()
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;

            var packages = packageRepository.Get();

            Assert.True(packages.Count() > 0);
        }

        [Fact]
        public async Task GetAllPackagesAsync_Test()
        {
            var context = GetContext();

            UnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;

            var packages = await packageRepository.GetAsync();

            Assert.True(packages.Count() > 0);
        }

        [Fact]
        public void GetPackagesByNumber_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            
            var packagesToQuery = packageRepository.Get(orderBy: o => o.OrderBy(p => p.Id), take: 8000);
            Assert.True(packagesToQuery.Count() == 8000);

            var trackingNumbers = packagesToQuery.Select(n => n.TrackingCode);

            var packages = packageRepository.Get(filter: p => trackingNumbers.Contains(p.TrackingCode));
            Assert.True(packages.Count() == 8000);            
        }

        [Fact]
        public async Task GetPackagesByNumberAsync_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;

            var packagesToQuery = await packageRepository.GetAsync(orderBy: o => o.OrderBy(p => p.Id), take: 8000);
            Assert.True(packagesToQuery.Count() == 8000);

            var trackingNumbers = packagesToQuery.Select(n => n.TrackingCode);

            var packages = await packageRepository.GetAsync(filter: p => trackingNumbers.Contains(p.TrackingCode));
            Assert.True(packages.Count() == 8000);
        }

        [Fact]
        public void UpdatePackageBulk_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            packageRepository.BulkOperations = new PackageBulkOperations(context);

            var packagesToUpdate = packageRepository.Get(orderBy: o => o.OrderBy(p => p.Id), take: 1000);
            Assert.True(packagesToUpdate.Count() == 1000);

            var updatedPackages = packagesToUpdate.Select(p => {
                p.FkStatusId = 2;
                return p;
            });

            packageRepository.BulkOperations.Update(updatedPackages);
        }
                
        [Fact]
        public async Task UpdatePackageAsync_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            packageRepository.BulkOperations = new PackageBulkOperations(context);

            var packagesToUpdate = await packageRepository.GetAsync(orderBy: o => o.OrderBy(p => p.Id), skip: 1000, take: 1000);
            Assert.True(packagesToUpdate.Count() == 1000);

            var updatedPackages = packagesToUpdate.Select(p => {
                p.FkStatusId = 2;
                return p;
            });

            await packageRepository.BulkOperations.UpdateAsync(updatedPackages);

        }

        [Fact]
        public void DeletePackageBulk_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            packageRepository.BulkOperations = new PackageBulkOperations(context);

            var packagesToDelete = packageRepository.Get(orderBy: o => o.OrderBy(p => p.Id), take: 1000);
            Assert.True(packagesToDelete.Count() == 1000);
                        
            packageRepository.BulkOperations.Delete(packagesToDelete);
        }

        [Fact]
        public async Task DeletePackageAsync_Test()
        {
            var context = GetContext();
            IUnitOfWork unitOfWork = new UnitOfWork(context);
            var packageRepository = unitOfWork.PackageRepository;
            packageRepository.BulkOperations = new PackageBulkOperations(context);

            var packagesToDelete = await packageRepository.GetAsync(orderBy: o => o.OrderBy(p => p.Id), take: 1000);
            Assert.True(packagesToDelete.Count() == 1000);

            await packageRepository.BulkOperations.DeleteAsync(packagesToDelete);
        }
                
        #endregion
    }
}
