﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using PackageHandler.API.DAL;
using PackageHandler.API.DTO;
using PackageHandler.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PackageHandler.API.Tests
{
    public class CheckpointServicesTest
    {
        private Mock<IConfiguration> GetConfigurationMock()
        {
            var mockConfSection = new Mock<IConfigurationSection>();
            //UPDATE THIS CONNECTION STRING!
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "DefaultConnection")])
                .Returns(@"data source=localhost\SQLEXPRESS2;Integrated Security=SSPI;initial catalog=db_tracker;Max Pool Size=1000;");

            var mockConfig = new Mock<IConfiguration>();
            mockConfig.Setup(c => c.GetSection(It.Is<string>(s => s == "ConnectionStrings")))
                .Returns(mockConfSection.Object);
            mockConfig.Setup(c => c["TrackerAPISettings:ReceivedStatusId"]).Returns("1");
            mockConfig.Setup(c => c["TrackerAPISettings:DefaultPageSizeS"]).Returns("5000");

            return mockConfig;
        }

        private TrackerContext GetContext()
        {
            var configuration = GetConfigurationMock().Object;
            var optionsBuilder = new DbContextOptionsBuilder<TrackerContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            var context = new TrackerContext(optionsBuilder.Options);

            return context;
        }

        [Fact]
        public async Task InsertCheckpoint_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());
            CheckpointValidator validator = new CheckpointValidator(unitOfWork);
            InsertCheckpoint insertCheckpoint = new InsertCheckpoint(unitOfWork, validator);

            var checkpoint = new InsertCheckpointDTO
            {
                Name = "TESTING A CHECKPOINT",
                City = "Test city",
                ControlTypeId = 1,
                PlaceTypeId = 1,
                CountryId = 7
            };

            await insertCheckpoint.CreateCheckpointAsync(checkpoint);            
        }

        [Fact]
        public async Task UpdateCheckpoint_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());
            CheckpointValidator validator = new CheckpointValidator(unitOfWork);
            UpdateCheckpoint updateCheckpoint = new UpdateCheckpoint(unitOfWork, validator);
                        
            var checkpoint = await unitOfWork.CheckpointRepository.GetByIDAsync(3);

            var checkpointDto = new UpdateCheckpointDTO
            {
                Id = 3,
                Name = checkpoint.Name,
                City = "Gotham city",
                ControlTypeId = checkpoint.FkControlTypeId,
                PlaceTypeId = checkpoint.FkPlaceTypeId,
                CountryId = checkpoint.FkCountryId
            };
            await updateCheckpoint.UpdateCheckpointAsync(checkpointDto);
        }

        [Fact]
        public async Task DeleteCheckpoint_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());
            CheckpointValidator validator = new CheckpointValidator(unitOfWork);
            DeleteCheckpoint deleteCheckpoint = new DeleteCheckpoint(unitOfWork);


            await deleteCheckpoint.DeleteCheckpointAsync(3);
        }
    }
}
