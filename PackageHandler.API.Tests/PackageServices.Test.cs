﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using PackageHandler.API.DAL;
using PackageHandler.API.DAL.Entities;
using PackageHandler.API.DTO;
using PackageHandler.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PackageHandler.API.Tests
{
    public class PackageServicesTest
    {

        private Mock<IConfiguration> GetConfigurationMock()
        {
            var mockConfSection = new Mock<IConfigurationSection>();
            //UPDATE THIS CONNECTION STRING!
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "DefaultConnection")])
                .Returns(@"data source=localhost\SQLEXPRESS2;Integrated Security=SSPI;initial catalog=db_tracker;Max Pool Size=1000;");

            var mockConfig = new Mock<IConfiguration>();
            mockConfig.Setup(c => c.GetSection(It.Is<string>(s => s == "ConnectionStrings")))
                .Returns(mockConfSection.Object);
            mockConfig.Setup(c => c["TrackerAPISettings:ReceivedStatusId"]).Returns("1");
            mockConfig.Setup(c => c["TrackerAPISettings:DefaultPageSizeS"]).Returns("5000");

            return mockConfig;
        }

        private TrackerContext GetContext()
        {
            var configuration = GetConfigurationMock().Object;
            var optionsBuilder = new DbContextOptionsBuilder<TrackerContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            var context = new TrackerContext(optionsBuilder.Options);

            return context;
        }
                
        #region Package

        [Fact]
        public async Task InsertPackages_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());
            PackageValidator validator = new PackageValidator(unitOfWork);
            InsertPackage insertPackage = new InsertPackage(unitOfWork, GetConfigurationMock().Object, validator);

            List<InsertPackageDTO> packageDTOs = new List<InsertPackageDTO>();
            for(int i = 0; i < 10000; i++)
            {
                packageDTOs.Add(new InsertPackageDTO
                {
                    IsoCountryCode = "PT",
                    DeliveryArea = i.ToString(),
                    CheckpointId = 1,
                    HasValueToPay = false,
                    IsFragile = false,
                    IsStoppedCustoms = false,
                    PackageValue = i,
                    PackageWeight = 4,
                    PackageWeightUnitId = 1,
                    PacketSizeId = 1,
                    ReceiveDate = DateTime.Now
                });
            }

            var trackingCodes = await insertPackage.CreatePackageAsync(packageDTOs);
            Assert.Equal(10000, trackingCodes.Count);
        }

        [Fact]
        public async Task UpdatePackages_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());
            PackageValidator validator = new PackageValidator(unitOfWork);
            UpdatePackage updatePackage = new UpdatePackage(unitOfWork, GetConfigurationMock().Object, validator);

            //Retrives 4500 packages
            var packages = await unitOfWork.PackageRepository.GetAsync(take: 4500);
            //Parse packages to update DTO, changing their status to in transit
            var packageDTOs = packages.Select(p => new UpdatePackageDTO
            {
                TrackingCode = p.TrackingCode,
                CheckpointId = p.FkCheckpointId,
                HasValueToPay = p.HasValueToPay,
                IsFragile = p.IsFragile,
                IsStoppedCustoms = p.IsStoppedCustoms,
                PackageStatusId = 2,
                PackageValue = p.PackageValue,
                PackageWeight = p.Weight,
                PackageWeightUnitId = p.FkWeightUnitId,
                PacketSizeId = p.FkPacketSizeId
            });
                        
            await updatePackage.UpdatePackageAsync(packageDTOs);            
        }

        [Fact]
        public async Task GetIntransitPackages_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());

            var inTransitPackages = await unitOfWork.PackageRepository.GetAsync(filter: p => p.FkStatusId == 2);

            Assert.Equal(4500, inTransitPackages.Count());
        }

        [Fact]
        public async Task DeletePackages_Test()
        {
            UnitOfWork unitOfWork = new UnitOfWork(GetContext());
            PackageValidator validator = new PackageValidator(unitOfWork);
            DeletePackage deletePackage = new DeletePackage(unitOfWork);

            //Retrives 5000 received packages
            var packages = await unitOfWork.PackageRepository.GetAsync(filter: o => o.FkStatusId == 1, take: 5000);
            //Get their tracking coded
            var trackingNumbers = packages.Select(p =>  p.TrackingCode);

            await deletePackage.DeletePackageAsync(trackingNumbers);
        }

        #endregion

    }
}
